<div class="tickevolution listing">
    <div class="row">
        <div class="col-md-4 filter">
            <div class="list-group times">
                <button type="button" class="list-group-item list-group-item-action active" disabled>Times</button>
                <button type="button" class="list-group-item list-group-item-action click" id="PM">Night</button>
                <button type="button" class="list-group-item list-group-item-action click" id="AM">Day</button>
            </div>
            <div class="list-group months">
                <button type="button" class="list-group-item list-group-item-action active" disabled>Months</button>
            </div>
            <div class="list-group days">
                <button type="button" class="list-group-item list-group-item-action active" disabled>Days</button>
            </div>
            <div class="list-group vanues">
                <button type="button" class="list-group-item list-group-item-action active" disabled>Vanues</button>
            </div>
        </div>
        <div class="col-md-8 list">
            <div class="items">
                <?php
                foreach ($arr_arg['arr_events'] as $event) {
                    ?>
                    <div class="shadow row title-event alert alert-secondary"
                         attr-times="<?= date("A", strtotime($event['occurs_at'])); ?>"
                         attr-months="<?= date("F", strtotime($event['occurs_at'])); ?>"
                         attr-days="<?= date("l", strtotime($event['occurs_at'])); ?>"
                         attr-vanues="<?= $event['venue']['name']; ?>">
                        <div class="col-md-3 centered" align="center">
                            <span class="badge badge-secondary list-mob-2">
                                <i class="fas fa-calendar-alt"></i>
                                 <?= ' ' . date("l", strtotime($event['occurs_at'])) . ' '; ?> 
                            </span><br>
                            <strong>
                                <span class="date-mob">
                              <?= date("M j", strtotime($event['occurs_at'])) . ', ' . date("Y", strtotime($event['occurs_at'])); ?>
                                </span>
                            </strong><br>
                            <span class="time">
                                <?= date("g:ia", strtotime($event['occurs_at'])); ?>
                            </span><br>
                        </div>
                        <div class="col-md-7">
                            <h3 id="title-event"><?= $event['name']; ?></h3>
                            <h4 class="gray"><i class="fas fa-map-marker-alt"></i>  <?= $event['venue']['name'] . ', ' . $event['venue']['location']; ?></h4>
                        </div>
                        <div class="col-md-2 float-right">
                            <a href="<?= $arr_arg['get_url_site'] . '/' . $arr_arg['get_slug_listing_event_page'] . '/' . $event['id'] . '/'; ?>" class="btn btn-success float-right boto" role="button" aria-pressed="true">Buy Tickets <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <?php
                }
                ?>  
            </div>
        </div>
    </div>
</div>
