<?php
/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    ticketevolution
 * @subpackage ticketevolution/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<?php

class TicketEvolutionAdminDisplay {

    public function content_page_admin_events_listing() {
        $GetApi = new GetApi();
        ?>
        <div class="wrap">
            <h2><?php echo get_admin_page_title() ?></h2>
            <pre>
                <?php
//                print_r($GetApi->get_api()->connectedApiTicket()->listTicketGroups([
//                            'event_id' => 1618646,
//                            'lightweight' => true
//                ]));

                print_r((array) $GetApi->get_show_list_events());
                ?>
            </pre>
        </div>
        <?php
    }

    public function content_page_admin_performers_listing() {
        $GetApi = new GetApi();
        ?>
        <div class="wrap">
            <h2><?php echo get_admin_page_title() ?></h2>
            <pre>
                <?php
                print_r((array) $GetApi->get_show_list_performers());
                ?>
            </pre>
        </div>
        <?php
    }

    public function content_page_admin_venues_listing() {
        $GetApi = new GetApi();
        ?>
        <div class="wrap">
            <h2><?php echo get_admin_page_title() ?></h2>
            <pre>
                <?php
                print_r((array) $GetApi->get_show_list_venues());
                ?>
            </pre>
        </div>
        <?php
    }

}
