<?php

use TicketEvolution\Client as TEvoClient;

/**
 * API connection Ticket Evolution for the plugin.
 *
 *
 * @package    ticketevolution
 * @subpackage ticketevolution/includes
 * 
 */
class TicketEvolutionConnectApi {

    protected static $_instance = null;

    public function __construct() {
        $this->connectedApiTicket();
    }

    /**
     * Ensuring there's only one instance
     */
    public static function instance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * Get data with API
     * @return array
     */
    public function connectedApiTicket() {

        require __DIR__ . '/../vendor/autoload.php';

        $ticket_evolution_page_settings = new TicketEvolutionPageSettings();

        $apiClient = new TEvoClient([
            'baseUrl' => $ticket_evolution_page_settings->getSettingsPageApi()['api_environment'],
            'apiVersion' => $ticket_evolution_page_settings->getSettingsPageApi()['api_version'],
            'apiToken' => $ticket_evolution_page_settings->getSettingsPageApi()['api_token'],
            'apiSecret' => $ticket_evolution_page_settings->getSettingsPageApi()['api_secret'],
        ]);

        return $apiClient;
    }
}
