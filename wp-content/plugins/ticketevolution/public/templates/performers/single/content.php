<div class="wrap">
    <h2><?= $arr_arg['name']; ?></h2>
    <pre>
        <?php
            //print_r($arr_arg['events']);
        ?>
    </pre>
  
</div>
<div class="tickevolution listing">
    <div class="row">
 
        <div class="col-md-12 list">
            <div class="items">
                        <?php
    $k = 0;
    foreach ($arr_arg['events']['events'] as $event) {
        ?>
                    <div class="row title-event alert alert-secondary"
                         attr-times="<?= date("A", strtotime($event['occurs_at'])); ?>"
                         attr-months="<?= date("F", strtotime($event['occurs_at'])); ?>"
                         attr-days="<?= date("l", strtotime($event['occurs_at'])); ?>"
                         attr-vanues="<?= $event['venue']['name']; ?>">
                       <!-- <div class="col-md-3 centered" align="center">
                            <span class="badge badge-secondary">
                                <i class="fas fa-calendar-alt"></i>
                                <?= ' ' . date("l", strtotime($event['occurs_at'])) . ' '; ?>
                            </span><br>
                            <strong>
                                <?= date("M j", strtotime($event['occurs_at'])) . ', ' . date("Y", strtotime($event['occurs_at'])); ?>
                            </strong><br>
                            <span class="time">
                                <?= date("g:ia", strtotime($event['occurs_at'])); ?>
                            </span><br>
                        </div> -->
                        <div class="col-md-9">
                            <h4 id="title-event"><?= $event['name'] . ' - venue: ' . $event['venue']['name'] ?></h4>
                             <!-- <h4 class="gray"><i class="fas fa-map-marker-alt"></i>  <?= $event['venue']['name'] . ', ' . $event['venue']['location']; ?></h4> -->
                        </div>
                        <div class="col-md-3 float-right">
                            <a href="<?= $arr_arg['get_url_site'] . '/' . $arr_arg['get_slug_listing_event_page'] . '/' . $event['id'] . '/'; ?>"class="btn btn-success float-right boto" role="button" aria-pressed="true">View Events <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <?php
                }
                ?>  
            </div>
        </div>
    </div>
</div>
