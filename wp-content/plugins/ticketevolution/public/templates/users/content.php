<div class="tickevolution row tickevolution-profile">
    <div class="col-md-12 alert alert-secondary">
        <div class="well profile">
            <div class="row">
                <div class="col-xs-12 col-sm-8">
                    <h2><?= $arr_arg['name'] ?></h2>
                    <span class="badge badge-pill badge-warning">balance: $<?= (int) $arr_arg['balance']; ?></span>
                    <p class="margin-0 font-size-18px"><strong>Telephone: </strong> 
                        <?php
                        if (count($arr_arg['phone_numbers']) == 0) {
                            echo 'empty';
                        } else {
                            foreach ($arr_arg['phone_numbers'] as $phone_numbers) {
                                if ($phone_numbers['primary'] == 1) {
                                    echo $phone_numbers['country_code'] . $phone_numbers['number'] . ' ';
                                }
                            }
                        }
                        ?>
                    </p>
                    <p class="margin-0 font-size-18px"><strong>Email: </strong> 
                        <?php
                        foreach ($arr_arg['email_addresses'] as $email) {
                            if ($email['primary'] == 1) {
                                echo $email['address'] . ' ';
                            }
                        }
                        ?> 
                    </p>
                    <p class="margin-0 font-size-18px"><strong>Address: </strong>
                        <?= $arr_arg['primary_shipping_address']['street_address'] . ' ' . $arr_arg['primary_shipping_address']['locality'] . ' ' . $arr_arg['primary_shipping_address']['region'] . ' ' . $arr_arg['primary_shipping_address']['postal_code'] . ' ' . $arr_arg['primary_shipping_address']['country_code']; ?>
                    </p>

                </div>             
                <div class="col-xs-12 col-sm-4 text-center">
                    <figure>
                        <i class="fas fa-user-astronaut fa-10x margin-1-rem" data-fa-transform="shrink-8"></i>
                    </figure>
                </div>
            </div>            
        </div>                 
    </div>
</div>



