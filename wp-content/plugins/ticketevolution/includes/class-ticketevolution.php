<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    ticketevolution
 * @subpackage ticketevolution/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    ticketevolution
 * @subpackage ticketevolution/includes
 * @author     Your Name <email@example.com>
 */
class TicketEvolution {

    /**
     * The loader that's responsible for maintaining and registering all hooks that power
     * the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      ticketevolution_Loader    $loader    Maintains and registers all hooks for the plugin.
     */
    protected $loader;

    /**
     * The unique identifier of this plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string    $ticketevolution    The string used to uniquely identify this plugin.
     */
    protected $ticketevolution;

    /**
     * The current version of the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string    $version    The current version of the plugin.
     */
    protected $version;
    private $require = []; // an array ofеу files to include ['file', 'scope']. 'scope' can be empty, 'admin' or 'public'

    /**
     * Define the core functionality of the plugin.
     *
     * Set the plugin name and the plugin version that can be used throughout the plugin.
     * Load the dependencies, define the locale, and set the hooks for the admin area and
     * the public-facing side of the site.
     *
     * @since    1.0.0
     */

    public function __construct() {
        if (defined('ticketevolution_VERSION')) {
            $this->version = ticketevolution_VERSION;
        } else {
            $this->version = '1.0.0';
        }
        $this->ticketevolution = 'ticketevolution';

        $this->require[] = 'includes/class-ticketevolution-loader';             // The class responsible for orchestrating the actions and filters of the core plugin.
        $this->require[] = 'includes/class-ticketevolution-i18n';               // The class responsible for defining internationalization functionality of the plugin.        
        $this->require[] = 'admin/class-ticketevolution-admin';                 // The class responsible for defining all actions that occur in the admin area.
        $this->require[] = 'public/class-ticketevolution-public';               // The class responsible for defining all actions that occur in the public-facing side of the site.
        $this->require[] = 'admin/class-ticketevolution-admin-options';         // The class creates a page with plugin settings
        $this->require[] = 'includes/class-ticketevolution-connect-api';        // The class API connection Ticket Evolution
        $this->require[] = 'admin/class-ticketevolution-admin-page-listing';         // The class creates a page events listing
        $this->require[] = 'admin/partials/ticketevolution-admin-display';
        $this->require[] = 'includes/class-tickevolution-routs-page';           //class-ticketevolution-get-api
        $this->require[] = 'includes/class-ticketevolution-get-api';
        $this->require[] = 'includes/class-ticketevolution-post-api';
        $this->require[] = 'public/partials/ticketevolution-public-display';
        $this->require[] = 'admin/class-ticketevolution-settings-for-shortcode';
        $this->require[] = 'admin/class-ticketevolution-settings-for-invite';
        $this->require[] = 'admin/class-ticketevolution-settings-for-style';
        $this->require[] = 'admin/class-ticketevolution-settings-for-events';
        $this->require[] = 'admin/class-ticketevolution-settings-for-performers';
        $this->require[] = 'admin/class-ticketevolution-settings-for-venues';
        $this->require[] = 'admin/class-ticketevolution-shortcode';


        $this->load_dependencies();
        $this->set_locale();
        $this->define_admin_hooks();
        $this->define_public_hooks();

        TicketEvolutionConnectApi::instance();
        TicketEvolutionPageSettingsShortcode::instance();    
        TicketEvolutionPageSettingsInvite::instance();
        TicketEvolutionPageSettingsStyle::instance();
        TicketEvolutionPageSettingsEvents::instance();
        TicketEvolutionPageSettingsPerformers::instance();
        TicketEvolutionPageSettingsVenues::instance();
    }

    /**
     * Load the required dependencies for this plugin.
     *
     * Include the following files that make up the plugin:
     *
     * - ticketevolution_Loader. Orchestrates the hooks of the plugin.
     * - ticketevolution_i18n. Defines internationalization functionality.
     * - ticketevolution_Admin. Defines all hooks for the admin area.
     * - ticketevolution_Public. Defines all hooks for the public side of the site.
     *
     * Create an instance of the loader which will be used to register the hooks
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */

    /**
     * Load the required dependencies for this plugin.
     */
    private function load_dependencies() {


        foreach ($this->require as $file) {
            require_once $this::get_path("{$file}.php");
        }

        $this->loader = new TicketEvolution_Loader();
    }

    /**
     * Get the absolute path from relative
     */
    static function get_path($path = '') {
        if (!empty($path)) {
            $path = ltrim($path, '/');
        }

        return plugin_dir_path(__DIR__) . $path;
    }

    /**
     * Get the absolute ulr from relative
     */
    static function get_url($path = '') {
        if (!empty($path)) {
            $path = ltrim($path, '/');
        }

        return plugin_dir_url(__DIR__) . $path;
    }

    /**
     * Define the locale for this plugin for internationalization.
     *
     * Uses the ticketevolution_i18n class in order to set the domain and to register the hook
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */
    private function set_locale() {

        $plugin_i18n = new TicketEvolution_i18n();

        $this->loader->add_action('plugins_loaded', $plugin_i18n, 'load_plugin_textdomain');
    }

    /**
     * Register all of the hooks related to the admin area functionality
     * of the plugin.
     *
     * @since    1.0.0
     * @access   private
     */
    private function define_admin_hooks() {

        $plugin_admin = new TicketEvolution_Admin($this->get_ticketevolution(), $this->get_version());

        $this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_styles');
        $this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts');

        $this->loader->add_action('user_register', $plugin_admin, 'add_user_in_api'); // Add user after added in DB site
        $this->loader->add_filter('user_contactmethods', $plugin_admin, 'client_contactmethods'); // Add new field in profile user
        $this->loader->add_filter('init', $plugin_admin, 'profile_login_redirect'); // redirect users after login form to her profile
        $this->loader->add_action('admin_notices', $plugin_admin, 'general_admin_notice');

        add_role('client_tevo_role', 'Client Ticketevolution', array('read' => true, 'level_0' => true, 'delete_posts' => false)); //Add role Client Ticketevolution
        
    }

    /**
     * Register all of the hooks related to the public-facing functionality
     * of the plugin.
     *
     * @since    1.0.0
     * @access   private
     */
    private function define_public_hooks() {

        $plugin_public = new TicketEvolution_Public($this->get_ticketevolution(), $this->get_version());

        $this->loader->add_action('wp_enqueue_scripts', $plugin_public, 'enqueue_styles');
        $this->loader->add_action('wp_enqueue_scripts', $plugin_public, 'enqueue_scripts');
        $this->loader->add_action('wp_head', $plugin_public, 'settings_css');
        $this->loader->add_action('wp_logout', $plugin_public, 'clear_cookie');


    }

    /**
     * Run the loader to execute all of the hooks with WordPress.
     *
     * @since    1.0.0
     */
    public function run() {
        $this->loader->run();
    }

    /**
     * The name of the plugin used to uniquely identify it within the context of
     * WordPress and to define internationalization functionality.
     *
     * @since     1.0.0
     * @return    string    The name of the plugin.
     */
    public function get_ticketevolution() {
        return $this->ticketevolution;
    }

    /**
     * The reference to the class that orchestrates the hooks with the plugin.
     *
     * @since     1.0.0
     * @return    ticketevolution_Loader    Orchestrates the hooks of the plugin.
     */
    public function get_loader() {
        return $this->loader;
    }

    /**
     * Retrieve the version number of the plugin.
     *
     * @since     1.0.0
     * @return    string    The version number of the plugin.
     */
    public function get_version() {
        return $this->version;
    }

}
