<?php

class TicketEvolutionPageSettingsStyle {

    protected static $_instance = null;

    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct() {
        add_action('admin_menu', array($this, 'add_style_page'));
        add_action('admin_init', array($this, 'page_init'));
    }

    /**
     * Add options page
     */
    public function add_style_page() {
        // This page will be under "Settings"
        add_submenu_page(
                'ticket_evolution_setting_admin', 'Generation Style', 'Style', 'manage_options', 'ticket_evolution_settings_style', array($this, 'create_style_page')
        );
    }

    /**
     * Ensuring there's only one instance
     */
    public static function instance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * Options page callback
     */
    public function create_style_page() {
        // Set class property
        $this->options = get_option('ticket_evolution_style_option');
        ?>
        <div class="wrap">
            <h1></h1>
            <form method="post" action="options.php">
                <?php
                // This prints out all hidden setting fields
                settings_fields('ticket_evolution_style_group');
                do_settings_sections('ticket_evolution_setting_style');
                submit_button();
                ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init() {
        register_setting(
                'ticket_evolution_style_group', // Option group
                'ticket_evolution_style_option', // Option name
                array($this, 'sanitize') // Sanitize
        );

        add_settings_section(
                'ticket_evolution_style_settings', // ID
                'Ticket Style Settings', // Title
                array($this, 'print_section_info'), // Callback
                'ticket_evolution_setting_style' // Page
        );
        
        add_settings_field('color_btn', 'Color Button', array($this, 'color_btn_callback'), 'ticket_evolution_setting_style', 'ticket_evolution_style_settings');
        
        add_settings_field('color_background', 'Color Background', array($this, 'color_background_callback'), 'ticket_evolution_setting_style', 'ticket_evolution_style_settings');
        
        add_settings_field('color_primery', 'Primery Color', array($this, 'color_primery_callback'), 'ticket_evolution_setting_style', 'ticket_evolution_style_settings');
        
        add_settings_field('color_text', 'Color Text', array($this, 'color_text_callback'), 'ticket_evolution_setting_style', 'ticket_evolution_style_settings');
        
        add_settings_field('secondary_color_text', 'Secondary Color Text', array($this, 'secondary_color_text_callback'), 'ticket_evolution_setting_style', 'ticket_evolution_style_settings');
    }

    /**'
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize($input) {
        $new_input = array();
        if (isset($input['color_btn']))
            $new_input['color_btn'] = sanitize_text_field($input['color_btn']);
        
        if (isset($input['color_background']))
            $new_input['color_background'] = sanitize_text_field($input['color_background']);
        
        if (isset($input['color_primery']))
            $new_input['color_primery'] = sanitize_text_field($input['color_primery']);
        
        if (isset($input['color_text']))
            $new_input['color_text'] = sanitize_text_field($input['color_text']);
        
        if (isset($input['secondary_color_text']))
            $new_input['secondary_color_text'] = sanitize_text_field($input['secondary_color_text']);

        return $new_input;
    }

    /**
     * Print the Section text
     */
    public function print_section_info() {
        print 'Enter your settings below:';
    }

    /**
     * Get the settings option array and print one of its values
     */
      public function color_btn_callback() {
        printf('<input type="color" id="color_btn" name="ticket_evolution_style_option[color_btn]" value="%s" />', isset($this->options['color_btn']) ? esc_attr($this->options['color_btn']) : '#28a745');
    }
    
    public function color_background_callback() {
        printf('<input type="color" id="color_background" name="ticket_evolution_style_option[color_background]" value="%s" />', isset($this->options['color_background']) ? esc_attr($this->options['color_background']) : '#e2e3e5');
    }
    
    public function secondary_color_text_callback() {
        printf('<input type="color" id="secondary_color_text" name="ticket_evolution_style_option[secondary_color_text]" value="%s" />', isset($this->options['secondary_color_text']) ? esc_attr($this->options['secondary_color_text']) : '#808080');
    }
    
    public function color_primery_callback() {
        printf('<input type="color" id="color_primery" name="ticket_evolution_style_option[color_primery]" value="%s" />', isset($this->options['color_primery']) ? esc_attr($this->options['color_primery']) : '#007bff');
    }
    
    public function color_text_callback() {
        printf('<input type="color" id="color_text" name="ticket_evolution_style_option[color_text]" value="%s" />', isset($this->options['color_text']) ? esc_attr($this->options['color_text']) : '#383d41');
    }
}
