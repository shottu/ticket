<div class="row tickevolution tickevolution-tabs">
    <div class="col-sm-12 col-lg-4">
        <div class="list-group" id="list-tab" role="tablist">
            <a class="mob-list list-group-item list-group-item-action active" id="list-home-list" data-toggle="list" href="#list-my-tickets" role="tab" aria-controls="tickets"><i class="fas fa-ticket-alt"></i><span class="fotir"> My Tickets</span></a>
            <a class="mob-list list-group-item list-group-item-action" id="list-profile-list" data-toggle="list" href="#list-profile" role="tab" aria-controls="profile"><i class="fas fa-id-card"></i> <span class="fotir">My Profile </span></a>
            <a class="mob-list list-group-item list-group-item-action" id="list-settings-list" data-toggle="list" href="#list-settings" role="tab" aria-controls="settings"><i class="fas fa-user-cog"></i> <span class="fotir">Settings </span></a>
            <a class="mob-list list-group-item list-group-item-action d-flex align-items-center" id="my-cart" data-toggle="list" href="#list-my-cart" role="tab" aria-controls="cart" style="height: 74px;"><span class="text-muted-1"><i class="fas fa-shopping-cart"></i><span class="fotir"> Your cart </span></span>
                <?php if (!empty($_COOKIE['tickets_user'])): ?>
                        <span class="badge badge-secondary badge-pill count-cart cart-mob ml-auto">
                        <?= count($arr_arg['cookies']); ?>
                    </span>
                <?php endif; ?>
                <!--    info box for js-->
                <!--end-->
            </a>
        </div>
    </div>
    <div class="col-sm-12 col-lg-8">
        <div class="tab-content" id="nav-tabContent">
            <div class="tab-pane fade show active" id="list-my-tickets" role="tabpanel" aria-labelledby="list-my-tickets">
                You have no tickets purchased
                <pre>
                    
                </pre>
            </div>
            <div class="tab-pane fade" id="list-profile" role="tabpanel" aria-labelledby="list-profile-list">
                <div class="col-md-12">
                    <div class="alert alert-secondary" role="alert">
                        <div class="container">
                            <div class="row">
                                <div class="col">
                                    <p class="margin-0 font-size-18px">
                                        <strong>ID:</strong> <?= $arr_arg['id']; ?> 
                                    </p>                                    
                                     <p class="margin-0 font-size-18px"><strong>Email: </strong> 
                                        <?php
                                        foreach ($arr_arg['email_addresses'] as $email) {
                                            if ($email['primary'] == 1) {
                                                echo $email['address'] . ' ';
                                            }
                                        }
                                        ?> 
                                    </p>
                                </div>
                                <div class="col">
                                    <p class="margin-0 font-size-18px">
                                        <strong>Name:</strong>  <?= $arr_arg['name']; ?>
                                    </p>
                                    <p class="margin-0 font-size-18px"><strong>Telephone: </strong> 
                                        <?php
                                        if (count($arr_arg['phone_numbers']) == 0) {
                                            echo 'empty';
                                        } else {
                                            foreach ($arr_arg['phone_numbers'] as $phone_numbers) {
                                                if ($phone_numbers['primary'] == 1) {
                                                    echo $phone_numbers['country_code'] . $phone_numbers['number'] . ' ';
                                                }
                                            }
                                        }
                                        ?>
                                    </p>
                                </div>
                                <div class="w-100"></div>
                                <div class="col">
                                    <p class="margin-0 font-size-18px"><strong>Address: </strong><br>
                                     
                                    </p>

                                </div>
                                <div class="col">
                                   <p class="margin-0 font-size-18px">
                                       <?= $arr_arg['primary_shipping_address']['street_address'] . ' ' . $arr_arg['primary_shipping_address']['locality'] . ' ' . $arr_arg['primary_shipping_address']['region'] . ' ' . $arr_arg['primary_shipping_address']['postal_code']; ?>
                                   </p>
                                </div>
                                <div class="w-100"></div>
                                <div class="col">
                                    <p class="margin-0 font-size-18px"><strong>Notes: </strong><br>
                                     
                                    </p>

                                </div>
                                <div class="col">
                                   <p class="margin-0 font-size-18px">
                                       <?php
                                       if($arr_arg['notes']) echo $arr_arg['notes'];
                                       else echo 'empty';
                                       ?>
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="alert alert-success" role="alert">
                        <p class="margin-0 font-size-18px">
                            <strong>Credit cart:</strong>
                            <?= $arr_arg['primary_credit_card']['name']?>
                            <?= $arr_arg['primary_credit_card']['expiration_month'] . ' ' . $arr_arg['primary_credit_card']['expiration_year']?>
                            <span class="badge badge-secondary badge-pill count-cart"><?= $arr_arg['primary_credit_card']['card_company']; ?></span>
                        </p>
                    </div>                    
                </div>
                <pre>
                    <?//= print_r($arr_arg['primary_credit_card']); ?>
                </pre>                 
            </div>
            <div class="tab-pane fade" id="list-settings" role="tabpanel" aria-labelledby="list-settings-list">
                <form action="<?= $_SERVER['REQUEST_URI']; ?>" method="post" name="form" class="needs-validation" novalidate>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <?php
                            foreach ($arr_arg['email_addresses'] as $email) {
                                if ($email['primary'] == 1) {
                                    $email = $email['address'];
                                }
                            }
                            ?> 
                            <label for="inputEmail4"><span class="fotyre">Email</span></label>
                            <input type="email" class="form-control" id="inputEmail4" placeholder="<?= $email; ?>" name="inputEmail4" value="<?= $email; ?> " required>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="inputCountryCodePhone"><span class="fotyre">Telephone </span></label>
                            <input type="text" class="form-control" id="inputCountryCodePhone" placeholder="+1" name="inputCountryCodePhone" value="<?= $arr_arg['primary_phone_number']['country_code']; ?>" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="inputNumberPhone"><span class="fotyre-me">&nbsp; </span></label>
                            <input type="text" class="form-control" id="inputNumberPhone" placeholder="541-555-0987" name="inputNumberPhone" value="<?= $arr_arg['primary_phone_number']['number']; ?>" minlength="9" maxlength="9" required>
                        </div>
                    </div>
                    <div class="form-row gare">
                        <div class="form-group col-md-5">
                            <label for="inputAddress"><span class="fotyre">Address </span></label>
                            <input type="text" class="form-control" id="inputAddress" placeholder="742 Evergreen Terrace" name="inputAddress" value="<?= $arr_arg['addresses'][0]['street_address']; ?>" required>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="inputLocality"><span class="fotyre">City </span></label>
                            <input type="text" class="form-control" id="inputLocality" placeholder="Springfield" name="inputLocality" value="<?= $arr_arg['addresses'][0]['locality']; ?>" required>
                        </div>
                        <div class="form-group col-md-1">
                            <label for="inputRegion"><span class="fotyre">State </span></label>
                            <input type="text" class="form-control" id="inputRegion" placeholder="OR" name="inputRegion" value="<?= $arr_arg['addresses'][0]['region']; ?>" required>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="inputPostalCode"><span class="fotyre">Zip </span></label>
                            <input type="number" class="form-control" id="inputPostalCode" placeholder="97475" name="inputPostalCode" value="<?= $arr_arg['addresses'][0]['postal_code']; ?>" required>
                        </div>
                        <div class="form-group col-md-2">
                            <label for="inputCountryCode"><span class="fotyre">Country </span></label>
                            <input type="text" class="form-control" id="inputCountryCode" placeholder="US" name="inputCountryCode" value="<?= $arr_arg['addresses'][0]['country_code']; ?>" required>
                        </div>
                    </div>
                    <?php
                    if ($arr_arg['show_danger'] == 'd-none') {
                        ?>
                        <div class="form-row visible credite-cart">
                            <div class="form-group col-md-3">
                                <label for="inputNameCart"><span class="fotyre">Name Cart</span></label>
                                <input type="text" class="form-control" id="inputNameCart" placeholder="Last Name and First Name" name="inputNameCart" value="<?= $arr_arg['primary_credit_card']['name'] ?>" required>
                            </div>
                            <div class="form-group col-md-3">
                                <label for="inputNamberCreditCart"><span class="fotyre">Number Credit Cart</span></label>
                                <input type="text" class="form-control" id="inputNamberCreditCart" placeholder="4111111111111111" name="inputNamberCreditCart" value="" required>
                            </div>
                            <div class="form-group col-md-2">
                                <label for="inputExpirationMonth"><span class="fotyre">Month</span></label>
                                <input type="number" class="form-control" id="inputExpirationMonth" placeholder="10" name="inputExpirationMonth" value="<?= $arr_arg['primary_credit_card']['expiration_month'] ?>" required>
                            </div>
                            <div class="form-group col-md-2">
                                <label for="inputExpirationYear"><span class="fotyre">Year</span></label>
                                <input type="number" class="form-control" id="inputExpirationYear" placeholder="2020" name="inputExpirationYear" value="<?= $arr_arg['primary_credit_card']['expiration_year'] ?>" required>
                            </div>
                            <div class="form-group col-md-2">
                                <label for="inputVerificationCode"><span class="fotyre">Code</span></label>
                                <input type="number" class="form-control" id="inputExpirationYear" placeholder="776" name="inputVerificationCode" value="<?= $arr_arg['primary_credit_card']['verification_code'] ?>" required>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="form-row <?= $arr_arg['show_danger']; ?> credite-cart-not">
                            <div class="col-md-12">
                                <div class="alert alert-danger" role="alert">
                                    Add your address and phone! To add a credit card.
                                </div>
                            </div>
                        </div>
                    <?php } ?>
                    <button type="submit" class="btn btn-primary btn-block btn-success">Save</button>
                </form>
            </div>
            <div class="tab-pane fade" id="list-my-cart" role="tabpanel" aria-labelledby="list-my-cart">
                <ul class="list-group mb-3 list-tikets">
                    <?= $arr_arg['items_for_tab_cart']; ?>    
                    <?php if (!empty($_COOKIE['tickets_user'])) : ?>
                        <li class="list-group-item list-mob d-flex justify-content-between lh-condensed" attr-total="total">
                            <div class="row marging-0-auto">
                                <div class="col-md-12">
                                    Total Price:  <strong><span class="total-price">$<?= $arr_arg['total_price_for_tab_cart']; ?></span></strong>
                                </div>
                            </div>
                        </li>
                    <?php else :?>
                        <div class="alert alert-danger" role="alert">Tickets empty!</div>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>
</div>