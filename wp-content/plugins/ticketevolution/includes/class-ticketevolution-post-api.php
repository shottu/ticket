<?php

class PostApi {

    public function get_api() {

        return new TicketEvolutionConnectApi();
    }

    public function get_std_сlass() {

        return new stdClass();
    }

    public function create_orders() {

        return $this->get_api()->connectedApiTicket()->createOrders($arr);
    }

    public function create_clients($name_user) {

        return $this->get_api()->connectedApiTicket()->createClients($name_user);
    }
    
    public function create_credit_card_client($arr) {

        return $this->get_api()->connectedApiTicket()->createClientCreditCards($arr);
    }
    
    public function update_credit_card_client($arr) {

        return $this->get_api()->connectedApiTicket()->updateClientCreditCard($arr);
    }

    public function update_email_client($arr) {

        return $this->get_api()->connectedApiTicket()->updateClientEmailAddress($arr);
    }
    
    public function update_address_client($arr) {

        return $this->get_api()->connectedApiTicket()->updateClientAddress($arr);
    }
    
    public function update_phone_client($arr) {

        return $this->get_api()->connectedApiTicket()->updateClientPhoneNumber($arr);
    }

}
