(function ($) {
    'use strict';
    jQuery(document).ready(function () {


        jQuery('button#shortcode-events').on('click', function () {

            var number_events = jQuery('form#shortcode-events input#number_events').val();
            var venue_id = jQuery('form#shortcode-events input#venue_id').val();
            var office_id = jQuery('form#shortcode-events input#office_id').val();
            var performer_id = jQuery('form#shortcode-events input#performer_id').val();
            var category_id = jQuery('form#shortcode-events input#category_id').val();
            var only_with_tickets = jQuery('form#shortcode-events select#only_with_tickets').val();
            var by_time = jQuery('form#shortcode-events select#by_time').val();

            if (venue_id != '') {
                venue_id = ' venue_id="' + venue_id + '" ';
            }
            if (office_id != '') {
                office_id = ' office_id="' + office_id + '" ';
            }
            if (performer_id != '') {
                performer_id = ' performer_id="' + performer_id + '" ';
            }
            if (category_id != '') {
                category_id = ' category_id="' + category_id + '" ';
            }
            if (number_events != '') {
                number_events = ' number="' + number_events + '" ';
            }
            if (only_with_tickets != 'not_choose') {
                only_with_tickets = ' only_with_tickets="' + only_with_tickets + '" ';
            } else {
                only_with_tickets = '';
            }
            if (by_time != 'not_choose') {
                by_time = ' by_time="' + by_time + '" ';
            } else {
                by_time = '';
            }

            jQuery('span#shortcode-events').html('<span class="text-success">[events' +
                    venue_id +
                    office_id +
                    performer_id +
                    category_id +
                    number_events +
                    only_with_tickets +
                    by_time + ']</span>');
        });


        jQuery('button#shortcode-venues').on('click', function () {

            var number_venues = jQuery('form#shortcode-venues input#number_venues').val();
            var name = jQuery('form#shortcode-venues input#name').val();
            var first_letter = jQuery('form#shortcode-venues input#first_letter').val();
            var order_by = jQuery('form#shortcode-venues input#order_by').val();

            if (number_venues != '') {
                number_venues = ' number_venues="' + number_venues + '" ';
            }

            if (name != '') {
                name = ' name="' + name + '" ';
            }

            if (first_letter != '') {
                first_letter = ' first_letter="' + first_letter + '" ';
            }

            if (order_by != '') {
                order_by = ' order_by="' + order_by + '" ';
            }

            jQuery('span#shortcode-venues').html('<span class="text-success">[venues' +
                    number_venues +
                    name +
                    first_letter +
                    order_by + ']</span>');
        });

        jQuery('button#shortcode-cart').on('click', function () {

            var fixed = jQuery('form#shortcode-cart select#fixed').val();
            var show_btn_redirect_checkout = jQuery('form#shortcode-cart select#show_btn_redirect_checkout').val();
            
            if (fixed != 'not_choose') {
                fixed = ' fixed="' + fixed + '" ';
            } else {
                fixed = '';
            }
            
            if (show_btn_redirect_checkout != 'not_choose') {
                show_btn_redirect_checkout = ' show_btn_redirect_checkout="' + show_btn_redirect_checkout + '" ';
            } else {
                show_btn_redirect_checkout = '';
            }

            jQuery('span#shortcode-cart').html('<span class="text-success">[cart' +
                    fixed +
                    show_btn_redirect_checkout +
                    ']</span>');
        });

        jQuery('button#shortcode-performers').on('click', function () {

            var number_performers = jQuery('form#shortcode-performers input#number_performers').val();
            var name = jQuery('form#shortcode-performers input#name').val();
            var first_letter = jQuery('form#shortcode-performers input#first_letter').val();
            var order_by = jQuery('form#shortcode-performers input#order_by').val();
            var venue_id = jQuery('form#shortcode-performers input#venue_id').val();
            var category_id = jQuery('form#shortcode-performers input#category_id').val();
            //var only_with_upcoming_events = jQuery('form#shortcode-performers input#only_with_upcoming_events').val();

            //console.log(only_with_upcoming_events);

            if (number_performers != '') {
                number_performers = ' number_performers="' + number_performers + '" ';
            }

            if (name != '') {
                name = ' name="' + name + '" ';
            }

            if (first_letter != '') {
                first_letter = ' first_letter="' + first_letter + '" ';
            }

            if (order_by != '') {
                order_by = ' order_by="' + order_by + '" ';
            }

            if (venue_id != '') {
                venue_id = ' venue_id="' + venue_id + '" ';
            }
            if (category_id != '') {
                category_id = ' category_id="' + category_id + '" ';
            }

//            if (only_with_upcoming_events != 'not_choose' || only_with_upcoming_events != "undefined") {
//                only_with_upcoming_events = ' only_with_upcoming_events="' + only_with_upcoming_events + '" ';
//            } else {
//                only_with_upcoming_events = '';
//            }

            jQuery('span#shortcode-performers').html('<span class="text-success">[performers' +
                    number_performers +
                    name +
                    first_letter +
                    order_by +
                    venue_id +
                    category_id +
                    // only_with_upcoming_events
                    ']</span>');
        });
    });

})(jQuery);
