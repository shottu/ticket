<?php
/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    ticketevolution
 * @subpackage ticketevolution/public/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->

<?php

class TicketevolutionPublicDisplay {

    /**
     * Get url site
     * 
     * @return string
     */
    
    public function PostApi() {
        return new PostApi();
    }
    
    public static function get_url_site() {

        $url = 'http' . ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ? 's' : '') . '://';
        $url = $url . $_SERVER['SERVER_NAME'];

        return $url;
    }
    
    /* Updating data client */
    
    public function update_email($arr_info_client, $client_id_in_api) {
        if (!empty($arr_info_client['email_addresses'])) {
            foreach ($arr_info_client['email_addresses'] as $email) {
                if ($email['primary'] == 1) {
                    $email_address_id = (int) $email['id'];
                }
            }
        }

        if (!empty($_POST['inputEmail4'])) {
            if (strlen($_POST['inputEmail4']) > 0) {
                $update_email_client = $this->PostApi()->update_email_client([
                    'client_id' => $client_id_in_api,
                    'email_address_id' => $email_address_id,
                    'label' => 'home',
                    'address' => $_POST['inputEmail4']
                ]);
            }
        }
    }
    
    public function update_telephone($arr_info_client, $client_id_in_api) {
        if (!empty($_POST['inputCountryCodePhone'])) {
            if (strlen($_POST['inputCountryCodePhone']) > 0) {
                $update_email_client = $this->PostApi()->update_phone_client([
                    'client_id' => $client_id_in_api,
                    'phone_number_id' => (int) $arr_info_client['primary_phone_number']['id'],
                    'label' => 'home',
                    'country_code' => $_POST['inputCountryCodePhone']
                ]);
            }
        }
        if (!empty($_POST['inputNumberPhone'])) {
            if (strlen($_POST['inputNumberPhone']) > 0) {
                if (strlen($_POST['inputNumberPhone']) == 9) {
                    $update_email_client = $this->PostApi()->update_phone_client([
                        'client_id' => $client_id_in_api,
                        'phone_number_id' => (int) $arr_info_client['primary_phone_number']['id'],
                        'label' => 'home',
                        'number' => $_POST['inputNumberPhone']
                    ]);
                } else
                    var_dump('error');
            }
        }
    }
    
    public function create_credit($arr_info_client, $client_id_in_api) {
        if (!empty($_POST['inputNameCart'])) {
            if (strlen($_POST['inputNameCart']) > 0) {
                $inputNameCart = $_POST['inputNameCart'];
            }
        }
        if (!empty($_POST['inputNamberCreditCart'])) {
            if (strlen($_POST['inputNamberCreditCart']) > 0) {
                $inputNumberCreditCart = $_POST['inputNamberCreditCart'];
            }
        }
        if (!empty($_POST['inputExpirationMonth'])) {
            if (strlen($_POST['inputExpirationMonth']) > 0) {
                $inputExpirationMonth = $_POST['inputExpirationMonth'];
            }
        }
        if (!empty($_POST['inputExpirationYear'])) {
            if (strlen($_POST['inputExpirationYear']) > 0) {
                $inputExpirationYear = $_POST['inputExpirationYear'];
            }
        }
        if (!empty($_POST['inputVerificationCode'])) {
            if (strlen($_POST['inputVerificationCode']) > 0) {
                $inputVerificationCode = $_POST['inputVerificationCode'];
            }
        }
        if (!empty($arr_info_client['addresses'])) {
            foreach ($arr_info_client['addresses'] as $address) {
                if ($address['primary'] == 1) {
                    $address_id = (int) $address['id'];
                }
            }
        }

        if (!empty($_POST['inputExpirationYear']) && !empty($_POST['inputExpirationMonth']) && !empty($_POST['inputNamberCreditCart']) && !empty($_POST['inputNameCart']) && !empty($_POST['inputVerificationCode'])) {

            $credit_card = $this->stdClass();
            $credit_card->name = $inputNameCart;
            $credit_card->number = $inputNumberCreditCart;
            $credit_card->expiration_month = $inputExpirationMonth;
            $credit_card->expiration_year = $inputExpirationYear;
            $credit_card->address_id = $address_id;
            $credit_card->primary = 1;
            $credit_card->phone_number_id = (int) $arr_info_client['primary_phone_number']['id'];
            $credit_card->verification_code = $inputVerificationCode;

            if ($arr_info_client['primary_credit_card']['primary'] != 1) {
                $this->PostApi()->create_credit_card_client([
                    'client_id' => $client_id_in_api,
                    'credit_cards' => [$credit_card]
                ]);
            } else {
                $this->PostApi()->update_credit_card_client([
                    'client_id' => $client_id_in_api,
                    'credit_card_id' => (int) $arr_info_client['primary_credit_card']['id'],
                    'name' => $inputNameCart,
                    'number' => $inputNumberCreditCart,
                    'expiration_month' => $inputExpirationMonth,
                    'expiration_year' => $inputExpirationYear,
                    'address_id' => $address_id,
                    'primary' => 1,
                    'phone_number_id' => (int) $arr_info_client['primary_phone_number']['id'],
                    'verification_code' => $inputVerificationCode
                ]);
            }
        }
    }
    
    public function update_addresses($arr_info_client, $client_id_in_api) {                
        try {
            if (!empty($arr_info_client['addresses'])) {
                foreach ($arr_info_client['addresses'] as $address) {
                    if ($address['primary'] == 1) {
                        $address_id = (int) $address['id'];
                    }
                }
            }

            if (!empty($_POST['inputAddress'])) {
                if (strlen($_POST['inputAddress']) > 0) {
                    $update_email_client = $this->PostApi()->update_address_client([
                        'client_id' => $client_id_in_api,
                        'address_id' => $address_id,
                        'label' => 'home',
                        'street_address' => $_POST['inputAddress']
                    ]);
                }
            }

            if (!empty($_POST['inputLocality'])) {
                if (strlen($_POST['inputLocality']) > 0) {
                    $update_email_client = $this->PostApi()->update_address_client([
                        'client_id' => $client_id_in_api,
                        'address_id' => $address_id,
                        'label' => 'home',
                        'locality' => $_POST['inputLocality']
                    ]);
                }
            }

            if (!empty($_POST['inputRegion'])) {
                if (strlen($_POST['inputRegion']) > 0) {
                    $update_email_client = $this->PostApi()->update_address_client([
                        'client_id' => $client_id_in_api,
                        'address_id' => $address_id,
                        'label' => 'home',
                        'region' => $_POST['inputRegion']
                    ]);
                }
            }

            if (!empty($_POST['inputPostalCode'])) {
                if (strlen($_POST['inputPostalCode']) > 0) {
                    $update_email_client = $this->PostApi()->update_address_client([
                        'client_id' => $client_id_in_api,
                        'address_id' => $address_id,
                        'label' => 'home',
                        'postal_code' => $_POST['inputPostalCode']
                    ]);
                }
            }

            if (!empty($_POST['inputCountryCode'])) {
                if (strlen($_POST['inputCountryCode']) > 0) {
                    $update_email_client = $this->PostApi()->update_address_client([
                        'client_id' => $client_id_in_api,
                        'address_id' => $address_id,
                        'label' => 'home',
                        'country_code' => $_POST['inputCountryCode']
                    ]);
                }
                echo '<div class="alert alert-success" role="alert">Your data is updated</div>';
            }
        } catch (Exception $e) {
            if ($e->getCode() == 1) {
                die($e->getMessage());
            } else {
                if (preg_match("/422 Unprocessable Entity/i", $e->getMessage())) {
                    echo "<div class='alert alert-danger' role='alert'>Error, invalid region</div>";
                }
            }
        }
    }
    
    /* end updating */

    /**
     * This function return slug listing page
     * 
     * @param type $page
     * @return type
     */
    public function get_slug_listing_page($page) {
        
        if (!empty(get_option('ticket_evolution_'.$page.'_option')["page_listing_$page"])) {
            return get_post(get_option('ticket_evolution_'.$page.'_option')["page_listing_$page"])->post_name;
        }
    }

    /**
     * This function return count pre page (events, venues, performers)
     * 
     * @param type $page
     * @return type
     */
    public function get_number_pre_page($page) {

        return (int) get_option('ticket_evolution_'.$page.'_option')["count_on_listing_$page"];
    }
    
    /**
     * This function return date show from(events, venues, performers)
     * 
     * @param type $page
     * @return type
     */
    public function get_date_from($page) {

        return get_option('ticket_evolution_'.$page.'_option')["date_from_$page"];
    }
    
    /**
     * This function return date show from(events, venues, performers)
     * 
     * @param type $page
     * @return type
     */
    public function get_date_to($page) {

        return get_option('ticket_evolution_'.$page.'_option')["date_to_$page"];
    }

    /**
     * 
     * @return string
     */
    public function get_id_with_url() {
        if (strpos($_SERVER['REQUEST_URI'], 'page') != false) {
            return 'this_pagination';
        } else {
            return (int) preg_replace("/[^0-9]/", '', $_SERVER['REQUEST_URI']);
        }
    }

    public static function url_template_folder() {
        return "public/templates/";
    }

    public function template($name_template, $arr) {

        $arr_arg = $arr;

        include TicketEvolution::get_path($this::url_template_folder() . $name_template . ".php");
    }

    /**
     * Return number current page for pagination
     * 
     * @return int
     */
    public static function current_page() {
        $current_page = (int) preg_replace("/[^0-9]/", '', $_SERVER['REQUEST_URI']);

        if ($current_page == 0) {
            $current_page = 1;
        }

        return $current_page;
    }

    public function check_user_page() {
        if (strpos($_SERVER['REQUEST_URI'], get_post(get_option('ticket_evolution_option')['page_user_profile'])->post_name) != false) {
            return true;
        } else {
            return false;
        }
    }

    public function render_items_li_for_cart($arr_cookies) {
        $commission = 0;
        $price_delivery = 0;
        $return = '';
        if(!empty($_COOKIE['tickets_user'])) {
        foreach ($arr_cookies as $key => $value) {

            $return .= '<li class="list-group-item list-mob justify-content-between lh-condensed" attr-id="' . $value['id'] . '">';
            $return .= '<div>';
            $return .= '<div class="row"><div class="col-md-10"><h6 class="title-event">' . str_replace("}", "", $value['title_event']) . '</h6></div><div class="col-md-2"><i class=" fa-sm close fas fa-times-circle float-right" attr-id="' . $value['id'] . '"></i></div></div>';
            $return .= '<p class="textfor text-muted"><span class="letor">Section: ' . $value['ticket_section'] . '</span></p><br>';
            $return .= '<p class="textfor text-muted"><span class="letor">Row: ' . $value['ticket_row'] . '</span></p><br>';

            if (!empty($value['delivery_options'])) {
                foreach ($value['delivery_options']['settings'] as $key => $setting) {
                    if ($setting['name'] == $value['info_ticket_group']['format']) {
                        $price_delivery = $setting['price'];
                    }
                }
            } else {
                $price_delivery = 0;
            }


            if(!empty($value['info_ticket_group'])) {
                $return .= '<p class="textfor text-muted"><span class="letor">Delivery Method: ' . $value['info_ticket_group']['format'] . ' +$' . $price_delivery . '</span></p><br>';
            }
            
            if(!empty($value['list_service_fee'])) {
                    foreach ($value['list_service_fee']['settings'] as $key => $setting) {
                        if ($value['price_ticket'] >= $setting['range_minimum']) {
                            if ($setting['type'] == 'PercentageServiceFee') {
                                $commission = ($value['price_ticket'] * $setting['amount']) / 100;
                            } else if ($setting['type'] == 'FlatServiceFee') {
                                $commission = $setting['amount'];
                            }
                            echo '<small class="textfor text-muted">' . $setting['label'] . ': +$' . $commission . '</small><br>';
                        }
                    }

                    $return .= '<small class="text-muted"><span class="badge badge-success">' . $value['count_ticket'] . ' Tickets </span></small>';
                    $return .= '<span class="text-muted price-all-tikets float-right">$' . ($value['price_ticket'] * $value['count_ticket']) . '</span>';
                    $return .= '</div>';
                }
            }
        }
        return $return;
    }

    public function total_price_for_cart($arr_cookies) {
        $total_price = 0;
        $commission = 0;
        $price_delivery = 0;
        if (!empty($arr_cookies)) {
            foreach ($arr_cookies as $key => $value) {
                
                if (!empty($value['delivery_options']['settings'])) {
                    foreach ($value['delivery_options']['settings'] as $key => $setting) {
                        if ($setting['name'] == $value['info_ticket_group']['format']) {
                            $price_delivery = $setting['price'];
                        }
                    }
                }
                
                if (!empty($value['list_service_fee'])) {
                    foreach ($value['list_service_fee']['settings'] as $key => $setting) {
                        if ($value['price_ticket'] >= $setting['range_minimum']) {
                            if ($setting['type'] == 'PercentageServiceFee') {
                                $commission = ($value['price_ticket'] * $setting['amount']) / 100;
                            } else if ($setting['type'] == 'FlatServiceFee') {
                                $commission = $setting['amount'];
                            }
                        }
                    }
                }

                $total_price += ($value['price_ticket'] * $value['count_ticket']) + $commission + $price_delivery;
            }
        }
        return $total_price;
    }
    
    /**
     * Show warning if not credit card (primary) in client
     * 
     * @param type $primary
     * @return string
     */
    public function show_danger_credit_cart($primary) {
        if ($primary != 1) {
            return '';
        } else {
            return 'd-none';
        }
    }  
    
    public static function current_date_iso_8601() {
        return date("c");
    }

}
