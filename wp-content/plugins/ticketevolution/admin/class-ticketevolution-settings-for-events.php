<?php

class TicketEvolutionPageSettingsEvents {

    protected static $_instance = null;

    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct() {
        add_action('admin_menu', array($this, 'add_events_page'));
        add_action('admin_init', array($this, 'page_init'));
    }

    /**
     * Add options page
     */
    public function add_events_page() {
        // This page will be under "Settings"
        add_submenu_page(
                'ticket_evolution_setting_admin', 'Generation Events', 'Events', 'manage_options', 'ticket_evolution_settings_events', array($this, 'create_events_page')
        );
    }

    /**
     * Ensuring there's only one instance
     */
    public static function instance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * Options page callback
     */
    public function create_events_page() {
        // Set class property
        $this->options = get_option('ticket_evolution_events_option');
        ?>
        <div class="wrap">
            <h1></h1>
            <form method="post" action="options.php">
                <?php
                // This prints out all hidden setting fields
                settings_fields('ticket_evolution_events_group');
                do_settings_sections('ticket_evolution_setting_events');
                submit_button();
                ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init() {
        register_setting(
                'ticket_evolution_events_group', // Option group
                'ticket_evolution_events_option', // Option name
                array($this, 'sanitize') // Sanitize
        );

        add_settings_section(
                'ticket_evolution_events_settings', // ID
                'Ticket Events Settings', // Title
                array($this, 'print_section_info'), // Callback
                'ticket_evolution_setting_events' // Page
        );

        add_settings_field('page_listing_events', 'Choose Page Listing Events', array($this, 'page_listing_callback'), 'ticket_evolution_setting_events', 'ticket_evolution_events_settings');

        add_settings_field('page_category_events', 'Choose Page Category Events', array($this, 'page_listing_category_callback'), 'ticket_evolution_setting_events', 'ticket_evolution_events_settings');

        add_settings_field('count_on_listing_events', 'Number of Events per page', array($this, 'count_events_on_listing_page_callback'), 'ticket_evolution_setting_events', 'ticket_evolution_events_settings');

        add_settings_field('date_from_events', 'Show Events by date (from)', array($this, 'date_from_events_callback'), 'ticket_evolution_setting_events', 'ticket_evolution_events_settings');
        
        add_settings_field('date_to_events', 'Show Vents by date (to)', array($this, 'date_to_events_callback'), 'ticket_evolution_setting_events', 'ticket_evolution_events_settings');
    }

    /**'
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize($input) {
        $new_input = array();

        if (isset($input['page_listing_events']))
            $new_input['page_listing_events'] = sanitize_text_field($input['page_listing_events']);

        if (isset($input['page_category_events']))
            $new_input['page_category_events'] = sanitize_text_field($input['page_category_events']);

        if (isset($input['count_on_listing_events']))
            $new_input['count_on_listing_events'] = sanitize_text_field($input['count_on_listing_events']);

         if (isset($input['date_from_events']))
            $new_input['date_from_events'] = sanitize_text_field($input['date_from_events']);

        if (isset($input['date_to_events']))
            $new_input['date_to_events'] = sanitize_text_field($input['date_to_events']);

        if (isset($input['sort_events']))
            $new_input['sort_events'] = sanitize_text_field($input['sort_events']);

        return $new_input;
    }

    /**
     * Print the Section text
     */
    public function print_section_info() {
        print 'Enter your settings below:';
    }

    /**
     * Get the settings option array and print one of its values
     */

    public function date_to_events_callback() {
        printf('<input type="date" id="date_to_events" name="ticket_evolution_events_option[date_to_events]" value="%s" />', isset($this->options['date_to_events']) ? esc_attr($this->options['date_to_events']) : '');
    }

    public function date_from_events_callback() {
        printf('<input type="date" id="date_from_events" name="ticket_evolution_events_option[date_from_events]" value="%s" />', isset($this->options['date_from_events']) ? esc_attr($this->options['date_from_events']) : '');
    }

    public function count_events_on_listing_page_callback() {
        printf('<input type="number" id="count_on_listing_events" name="ticket_evolution_events_option[count_on_listing_events]" value="%s" />', isset($this->options['count_on_listing_events']) ? esc_attr($this->options['count_on_listing_events']) : '30');
    }

    public function page_listing_callback() {

        echo "<select id='page_listing_events' name='ticket_evolution_events_option[page_listing_events]'>";
        echo '<option value="">' . esc_attr(__('Select page')) . '</option>';

        foreach (get_pages() as $page) {

            $selected = (get_option('ticket_evolution_events_option')['page_listing_events'] == $page->ID) ? 'selected="selected"' : '';
            echo '<option value="' . $page->ID . '"' . $selected . '>' . esc_html($page->post_title) . '</option>';
        }
        echo "</select>";
    }

    public function sort_events_callback() {

        $items = [
            "Upcoming events" => "ASC",
            "Latest events" => "DESC",
        ];

        $sort_events = isset($this->options['sort_events']) ? esc_attr($this->options['sort_events']) : '';

        echo "<select id='sort_events' name='ticket_evolution_events_option[sort_events]'>";
        foreach ($items as $item => $value) {
            $selected = ($sort_events == $value) ? 'selected="selected"' : '';
            echo "<option value='$value' $selected>$item</option>";
        }
        echo "</select>";
    }

    public function page_listing_category_callback() {

        echo "<select id='page_category_events' name='ticket_evolution_events_option[page_category_events]'>";
        echo '<option value="">' . esc_attr(__('Select page')) . '</option>';

        foreach (get_pages() as $page) {

            $selected = (get_option('ticket_evolution_events_option')['page_category_events'] == $page->ID) ? 'selected="selected"' : '';
            echo '<option value="' . $page->ID . '"' . $selected . '>' . esc_html($page->post_title) . '</option>';
        }
        echo "</select>";
    }

}
