<?php

class TicketEvolutionPageSettingsShortcode {

    protected static $_instance = null;

    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options_events;
    private $options_venues;

    /**
     * Start up
     */
    public function __construct() {
        add_action('admin_menu', array($this, 'add_shortcode_page'));
    }

    /**
     * Add options page
     */
    public function add_shortcode_page() {
        // This page will be under "Settings"
        add_submenu_page(
                'ticket_evolution_setting_admin', 'Generation shortcode', 'Shortcode', 'manage_options', 'ticket_evolution_settings_shortcode', array($this, 'create_shortcode_page')
        );
    }

    /**
     * Ensuring there's only one instance
     */
    public static function instance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * Options page callback
     */
    public function create_shortcode_page() {
        ?>
        <div class="wrap">
            <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="nav-events-tab" data-toggle="tab" href="#nav-events" role="tab" aria-controls="events"><i class="fas fa-calendar-week"></i> Events</a>
                    <a class="nav-item nav-link" id="nav-venues-tab" data-toggle="tab" href="#nav-venues" role="tab" aria-controls="venues"><i class="fas fa-map-signs"></i> Venues</a>
                    <a class="nav-item nav-link" id="nav-performers-tab" data-toggle="tab" href="#nav-performers" role="tab" aria-controls="performers"><i class="fas fa-user-tie"></i> Performers</a>
                    <a class="nav-item nav-link" id="nav-cart-tab" data-toggle="tab" href="#nav-cart" role="tab" aria-controls="cart"><i class="fas fa-shopping-cart"></i> Cart</a>
                </div>
            </nav>
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-events" role="tabpanel" aria-labelledby="nav-events-tab">
                    <h1>Generation shortcode for events</h1>
                    <form id="shortcode-events">
                        <div class="form-group row">
                            <label for="number_events" class="col-sm-2 col-form-label">Number</label>
                            <div class="col-sm-10">
                                <input type="number" max="100" class="form-control" id="number_events" placeholder="Number of events (max 100 events)">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="venue_id" class="col-sm-2 col-form-label">Venue Id</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" id="venue_id" placeholder="Venue at which the event occurs">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="office_id" class="col-sm-2 col-form-label">Office Id</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" id="office_id" placeholder="Show only events for which this office has tickets">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="performer_id" class="col-sm-2 col-form-label">Performer Id</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" id="performer_id" placeholder="Show only events at which this performer will perform">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="category_id" class="col-sm-2 col-form-label">Category Id</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" id="category_id" placeholder="Show only events in this category (does not include sub-categories)">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-2">
                                <label for="only_with_tickets">Only with tickets</label>
                            </div>
                            <div class="col-sm-10">
                                <select class="custom-select" id="only_with_tickets">
                                    <option value="not_choose" selected>Choose...</option>
                                    <option value="false">No</option>
                                    <option value="true">Yes</option>
                                </select>
                                <small id="only_with_ticketsHelpBlock" class="form-text text-muted">
                                    Show only events that have inventory irregardless of whether they are available for sale (All states will be shown).
                                </small>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-2">
                                <label for="by_time">By time</label>
                            </div>
                            <div class="col-sm-10">
                                <select class="custom-select" id="by_time">
                                    <option value="not_choose" selected>Choose...</option>
                                    <option value="day">Day</option>
                                    <option value="night">Night</option>
                                </select>
                                <small id="only_with_ticketsHelpBlock" class="form-text text-muted">
                                    Show only events whose start time is during the day (between 4 AM-4 PM) or night (otherwise)
                                </small>
                            </div>
                        </div>
                    </form>
                    <div class="form-group row">
                        <div class="col-sm-2">
                            <button type="submit" class="btn btn-primary" id="shortcode-events">Get shortcode</button>
                        </div>
                        <div class="col-sm-10">
                            <div class="alert alert-dark" role="alert">
                                <span id="shortcode-events"><span class="text-success">[events]</span> - This shortcode show 100 events</span>
                                <small id="only_with_ticketsHelpBlock" class="form-text text-muted">
                                    All shortcode arguments can be found <a href="https://ticketevolution.atlassian.net/wiki/spaces/API/pages/9470092/Events+Index">here</a>.
                                </small>
                            </div>
                        </div>
                    </div>

                    <hr>
                </div>
                <div class="tab-pane fade" id="nav-venues" role="tabpanel" aria-labelledby="nav-venues-tab">
                    <h1>Generation shortcode for venues</h1>
                    <form id="shortcode-venues">
                        <div class="form-group row">
                            <label for="number_venues" class="col-sm-2 col-form-label">Number</label>
                            <div class="col-sm-10">
                                <input type="number" max="100" class="form-control" id="number_venues" placeholder="Number of venues (max 100 venues)">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label">Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="name" placeholder="Selby Stadium">
                                <small id="only_with_ticketsHelpBlock" class="form-text text-muted">
                                    Exact name of the Venue.
                                </small>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="first_letter" class="col-sm-2 col-form-label">First Letter</label>
                            <div class="col-sm-10">
                                <input type="text" maxlength="1" class="form-control" id="first_letter" placeholder="A">
                                <small id="only_with_ticketsHelpBlock" class="form-text text-muted">
                                    Filters venues by the first letter passed.
                                </small>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="order_by" class="col-sm-2 col-form-label">Order By</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="order_by" placeholder="venues.popularity_score DESC">
                                <small id="only_with_ticketsHelpBlock" class="form-text text-muted">
                                    Pass in the parameter that you want to order by and the sorting order (Ex. :order_by => "venues.popularity_score DESC")
                                </small>
                            </div>
                        </div>
                    </form>
                    <div class="form-group row">
                        <div class="col-sm-2">
                            <button type="submit" class="btn btn-primary" id="shortcode-venues">Get shortcode</button>
                        </div>
                        <div class="col-sm-10">
                            <div class="alert alert-dark" role="alert">
                                <span id="shortcode-venues"><span class="text-success">[venues]</span> - This shortcode show 100 venues</span>
                                <small id="only_with_ticketsHelpBlock" class="form-text text-muted">
                                    All shortcode arguments can be found <a href="https://ticketevolution.atlassian.net/wiki/spaces/API/pages/9470018/Venues+Index">here</a>.
                                </small>
                            </div>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="tab-pane fade" id="nav-performers" role="tabpanel" aria-labelledby="nav-performers-tab">
                    <h1>Generation shortcode for performers</h1>
                    <form id="shortcode-performers">
                        <div class="form-group row">
                            <label for="number_performers" class="col-sm-2 col-form-label">Number</label>
                            <div class="col-sm-10">
                                <input type="number" max="100" class="form-control" id="number_performers" placeholder="Number of performers (max 100 performers)">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label">Name</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="name" placeholder="Rolling Stones">
                                <small id="only_with_ticketsHelpBlock" class="form-text text-muted">
                                    Exact name of the Venue.
                                </small>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="first_letter" class="col-sm-2 col-form-label">First Letter</label>
                            <div class="col-sm-10">
                                <input type="text" maxlength="1" class="form-control" id="first_letter" placeholder="A">
                                <small id="only_with_ticketsHelpBlock" class="form-text text-muted">
                                    Filters venues by the first letter passed.
                                </small>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="order_by" class="col-sm-2 col-form-label">Order By</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="order_by" placeholder="venues.popularity_score DESC">
                                <small id="only_with_ticketsHelpBlock" class="form-text text-muted">
                                    Pass in the parameter that you want to order by and the sorting order (Ex. :order_by => "performers.popularity_score DESC")
                                </small>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="venue_id" class="col-sm-2 col-form-label">Venue Id</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" id="venue_id" placeholder="">
                                <small id="only_with_ticketsHelpBlock" class="form-text text-muted">
                                    ID of the Performer’s home venue (most performers do not have one)
                                </small>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="category_id" class="col-sm-2 col-form-label">Category Id</label>
                            <div class="col-sm-10">
                                <input type="number" class="form-control" id="category_id" placeholder="">
                                <small id="only_with_ticketsHelpBlock" class="form-text text-muted">
                                    Show only Performers in this category (does not include sub-categories).
                                </small>
                            </div>
                        </div>
                        <!--                        <div class="form-group row">
                                                    <div class="col-sm-2">
                                                        <label for="only_with_upcoming_events">Only with performers</label>
                                                    </div>
                                                    <div class="col-sm-10">
                                                        <select class="custom-select" id="only_with_upcoming_events">
                                                            <option value="not_choose" selected>Choose...</option>
                                                            <option value="false">No</option>
                                                            <option value="true">Yes</option>
                                                        </select>
                                                        <small id="only_with_ticketsHelpBlock" class="form-text text-muted">
                                                            Filter to only Performers who have upcoming events.
                                                        </small>
                                                    </div>
                                                </div>-->
                    </form>
                    <div class="form-group row">
                        <div class="col-sm-2">
                            <button type="submit" class="btn btn-primary" id="shortcode-performers">Get shortcode</button>
                        </div>
                        <div class="col-sm-10">
                            <div class="alert alert-dark" role="alert">
                                <span id="shortcode-performers"><span class="text-success">[performers]</span> - This shortcode show 100 performers</span>
                                <small id="only_with_ticketsHelpBlock" class="form-text text-muted">
                                    All shortcode arguments can be found <a href="https://ticketevolution.atlassian.net/wiki/spaces/API/pages/9470084/Performers+Index">here</a>.
                                </small>
                            </div>
                        </div>
                    </div>  
                    <hr>
                </div>
                <div class="tab-pane fade" id="nav-cart" role="tabpanel" aria-labelledby="nav-cart-tab">
                    <h1>Generation shortcode for cart</h1>
                    <form id="shortcode-cart">
                        <div class="form-group row">
                            <div class="col-sm-2">
                                <label for="fixed" class="col-form-label">Fixed items</label>
                            </div>
                            <div class="col-sm-10">
                                <select class="custom-select" id="fixed">
                                    <option value="not_choose" selected>Choose...</option>
                                    <option value="true">Yes</option>
                                    <option value="false">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-2">
                                <label for="show_btn_redirect_checkout" class="col-form-label">Show button redirect to checkout page</label>
                            </div>
                            <div class="col-sm-10">
                                <select class="custom-select" id="show_btn_redirect_checkout">
                                    <option value="not_choose" selected>Choose...</option>
                                    <option value="true">Yes</option>
                                    <option value="false">No</option>
                                </select>
                            </div>
                        </div>
                    </form>
                    <div class="form-group row">
                        <div class="col-sm-2">
                            <button type="submit" class="btn btn-primary" id="shortcode-cart">Get shortcode</button>
                        </div>
                        <div class="col-sm-10">
                            <div class="alert alert-dark" role="alert">
                                <span id="shortcode-cart"><span class="text-success">[cart]</span> - This shortcode show cart on any page</span>
                                <small id="only_with_ticketsHelpBlock" class="form-text text-muted">

                                </small>
                            </div>
                        </div>
                    </div>  
                    <hr>
                </div>
            </div>
        </div>
        <?php
    }

}
