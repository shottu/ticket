<?php

/**
 * The admin-specific functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    ticketevolution
 * @subpackage ticketevolution/admin
 */

/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    ticketevolution
 * @subpackage ticketevolution/admin
 * @author     Your Name <email@example.com>
 */
class TicketEvolution_Admin {

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $ticketevolution    The ID of this plugin.
     */
    private $ticketevolution;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $ticketevolution       The name of this plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($ticketevolution, $version) {

        $this->ticketevolution = $ticketevolution;
        $this->version = $version;
    }

    public function PostApi() {
        return new PostApi();
    }
    
    public function TicketevolutionPublicDisplay() {
        return new TicketevolutionPublicDisplay();
    }
    

    /**
     * Register the stylesheets for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_styles() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in ticketevolution_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The ticketevolution_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_enqueue_style($this->ticketevolution, plugin_dir_url(__FILE__) . 'css/ticketevolution-admin.css', array(), $this->version, 'all');
        wp_enqueue_style('bootstrap-grid', plugin_dir_url(__FILE__) . 'css/bootstrap-grid.min.css');
        wp_enqueue_style('bootstrap', plugin_dir_url(__FILE__) . 'css/bootstrap.min.css');
        wp_enqueue_style('awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css');
    }

    /**
     * Register the JavaScript for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in ticketevolution_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The ticketevolution_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_enqueue_script($this->ticketevolution, plugin_dir_url(__FILE__) . 'js/ticketevolution-admin.js', array('jquery'), $this->version, false);
        wp_enqueue_script('bootstrap', plugin_dir_url(__FILE__) . 'js/bootstrap.min.js', array('jquery'), 4);
        wp_enqueue_script('bundle', plugin_dir_url(__FILE__) . 'js/bootstrap.bundle.min.js');
    }

    /**
     * This function added users after registration on site in API Tickevolution
     * 
     * @param type $client_id
     */
    public function add_user_in_api($client_id) {

        $client_info = get_userdata($client_id);

        $client = $this->PostApi()->get_std_сlass();

        $client->name = $client_info->display_name;

        $email_address_work = $this->PostApi()->get_std_сlass();
        $email_address_work->label = 'work';
        $email_address_work->address = $client_info->user_email;
        $email_address_work->is_primary = true;

        $street_address_work = $this->PostApi()->get_std_сlass();
        $street_address_work->label = 'home';
        $street_address_work->name = '';
        $street_address_work->company = '';
        $street_address_work->street_address = '';
        $street_address_work->extended_address = '';
        $street_address_work->locality = '';
        $street_address_work->region = '';
        $street_address_work->postal_code = '';
        $street_address_work->country_code = '';
        $street_address_work->is_primary_shipping = true;
        $street_address_work->is_primary_billing = true;

        $phone_number_home = $this->PostApi()->get_std_сlass();
        $phone_number_home->label = 'home';
        $phone_number_home->country_code = '';
        $phone_number_home->number = '';
        $phone_number_home->extension = null;
        $phone_number_home->is_primary = true;

        $company = $this->PostApi()->get_std_сlass();
        $company->name = '';

        $client->email_addresses = [$email_address_work];
        $client->addresses = [$street_address_work];
        $client->phone_numbers = [$phone_number_home];
        $client->company = $company;

        $response = $this->PostApi()->create_clients(['clients' => [$client]]); // add user in api
        
        /* registration on the page_login_users */
        $page_login_users = '/'. get_post(get_option('ticket_evolution_option')['page_login_users'])->post_name .'/';
        
        if ($page_login_users == parse_url($_SERVER['HTTP_REFERER'], PHP_URL_PATH)) {
            //create role client_tevo_role
            $role = wp_update_user([
                'ID' => $client_id,
                'role' => 'client_tevo_role',
            ]);
        }
        
        $update_client_id_in_metadata = update_user_meta($client_id, 'client_id', $response['clients'][0]['id']);
    }
    
    /**
     * This function for filter user_contactmethods
     * 
     * @param array $client_contactmethods
     * @return string
     */
    function client_contactmethods($client_contactmethods) {

        $client_contactmethods['client_id'] = 'Client ID in Ticketevolution';

        return $client_contactmethods;
    }
    
    function profile_login_redirect() {
        if (home_url('/' . $_SERVER['REQUEST_URI']) == home_url('/' . get_post(get_option('ticket_evolution_option')['page_login_users'])->post_name . '/') && is_user_logged_in() && current_user_can('client_tevo_role')) {
            wp_safe_redirect(home_url('/' . get_post(get_option('ticket_evolution_option')['page_user_profile'])->post_name . '/' . get_user_meta(get_current_user_id(), 'client_id')[0] . '/'));
            die();
        }
        if (home_url('/' . $_SERVER['REQUEST_URI']) == home_url('/wp-admin/profile.php') && is_user_logged_in() && current_user_can('client_tevo_role')) {
            wp_safe_redirect(home_url('/' . get_post(get_option('ticket_evolution_option')['page_user_profile'])->post_name . '/' . get_user_meta(get_current_user_id(), 'client_id')[0] . '/'));
            die();
        }
        if (home_url('/' . $_SERVER['REQUEST_URI']) == home_url('/wp-admin/') && is_user_logged_in() && current_user_can('client_tevo_role')) {
            wp_safe_redirect(home_url('/' . get_post(get_option('ticket_evolution_option')['page_user_profile'])->post_name . '/' . get_user_meta(get_current_user_id(), 'client_id')[0] . '/'));
            die();
        }
        if (home_url('/' . $_SERVER['REQUEST_URI']) == home_url('/wp-admin/about.php') && is_user_logged_in() && current_user_can('client_tevo_role')) {
            wp_safe_redirect(home_url('/' . get_post(get_option('ticket_evolution_option')['page_user_profile'])->post_name . '/' . get_user_meta(get_current_user_id(), 'client_id')[0] . '/'));
            die();
        }
        
        if(home_url('/' . $_SERVER['REQUEST_URI']) == home_url('/' . get_post(get_option('ticket_evolution_option')['page_user_profile'])->post_name . '/' . $this->TicketevolutionPublicDisplay()->get_id_with_url() . '/')) {
            if(!is_user_logged_in() || wp_get_current_user()->roles[0] != 'client_tevo_role' || $this->TicketevolutionPublicDisplay()->get_id_with_url() != get_user_meta(wp_get_current_user()->ID)['client_id'][0]) {
                wp_safe_redirect('/');
                die();
            }
        }
    }

    function arr_option() {

        return [ // array option page
            'ticket_evolution_option' => [
                'page_checkout' => 'Choose Page Checkout',
                'page_user_profile' => 'Choose Page Profile User',
                'page_login_users' => 'Choose Page Login and Registration Users'
            ],
            'ticket_evolution_events_option' => [
                'page_listing_events' => 'Choose Page Listing Events',
                'page_category_events' => 'Choose Page Category Events',
            ],
            'ticket_evolution_performers_option' => [
                'page_listing_performers' => 'Choose Page Listing Performers',
            ],
            'ticket_evolution_venues_option' => [
                'page_listing_venues' => 'Choose Page Listing Venues',
            ],
            'ticket_evolution_invite_option' => [
                'api_version' => 'Api Version',
                'api_token' => 'Your Api Token',
                'api_secret' => 'Your Api Secret',
            ],                  
        ];
    }

    function general_admin_notice() {

        $option = $this->arr_option(); // array option

        $result = false; // result get_option()[array]

        foreach ($option as $page => $val)
            foreach ($option[$page] as $key => $value)
                if (empty(get_option($page)[$key])) $result = true;
            
        if ($result) {
            echo '<div class="admin notice alert alert-warning alert-dismissible fade show" role="alert">
                  <strong>TicketEvolution: </strong> for normal operation of the plugin you need to fill in the fields';

            echo '<a class="alert-link" href="/wp-admin/admin.php?page=ticket_evolution_setting_admin">';

            foreach ($option as $page => $val)
                foreach ($option[$page] as $key => $value)
                    if (empty(get_option($page)[$key])) echo ' "' . $value . '" ';

            echo '</a>';

            echo '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>';

            echo '</div>';
        }
    }

}
