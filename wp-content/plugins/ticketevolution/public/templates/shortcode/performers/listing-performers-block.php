<div class="tickevolution listing">
    <div class="row">
        <div class="col-md-12 list">
            <div class="items">
                <?php
                foreach ($arr_arg['performers'] as $performers) {
                    ?>
                    <div class="row title-event alert alert-secondary"
                         attr-times="<?= date("A", strtotime($performers['occurs_at'])); ?>"
                         attr-months="<?= date("F", strtotime($performers['occurs_at'])); ?>"
                         attr-days="<?= date("l", strtotime($performers['occurs_at'])); ?>"
                         attr-vanues="<?= $event['venue']['name']; ?>">
                        <div class="col-md-9">
                            <h4 id="title-event"><?= $performers['name'] ?></h4>
                        </div>
                        <div class="col-md-3 float-right">
                            <a href="<?= $arr_arg['get_url_site'] . '/' . $arr_arg['get_slug_listing_performers_page'] . '/' . $performers['id'] . '/'; ?>"class="btn btn-success float-right" role="button" aria-pressed="true">View Events <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <?php
                }
                ?>  
            </div>
        </div>
    </div>
</div>

