<?php

/**
 * Test Class for view array with Api
 */

class TicketEvolutionPageListingAdmin {

    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct() {
        add_action('admin_menu', array($this, 'add_page_listing'));
    }

    /**
     * Add options page 
     */
    public function add_page_listing() {
        add_menu_page('Events Listing', 'Events Listing', 'manage_options', 'events_listing', array($this, 'content_page_admin_events_listing'), 'dashicons-calendar-alt', 4);
        add_menu_page('Performers Listing', 'Performers Listing', 'manage_options', 'performers_listing', array($this, 'content_page_admin_performers_listing'), 'dashicons-calendar-alt', 4);
        add_menu_page('Venues Listing', 'Venues Listing', 'manage_options', 'venues_listing', array($this, 'content_page_admin_venues_listing'), 'dashicons-calendar-alt', 4);
    }
    
    /**
     * Display table with events on admin page Events Listing
     */
    public function content_page_admin_events_listing() {
        $TicketEvolutionAdminDisplay = new TicketEvolutionAdminDisplay();
        $TicketEvolutionAdminDisplay->content_page_admin_events_listing();
    }
    
    /**
     * Display table with events on admin page Performers Listing
     */
    public function content_page_admin_performers_listing() {
        $TicketEvolutionAdminDisplay = new TicketEvolutionAdminDisplay();
        $TicketEvolutionAdminDisplay->content_page_admin_performers_listing();
    }
    
    /**
     * Display table with events on admin page Venues Listing
     */
    public function content_page_admin_venues_listing() {
        $TicketEvolutionAdminDisplay = new TicketEvolutionAdminDisplay();
        $TicketEvolutionAdminDisplay->content_page_admin_venues_listing();
    }

}

if(is_admin()){
  $TicketEvolutionPageListingAdmin = new TicketEvolutionPageListingAdmin();
}
