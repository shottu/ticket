<div class="tickevolution tickevolution-event">

    <!--    Title event-->
    <div class="row title-event alert alert-secondary">
        <div class="col-md-2 centered" align="left">
            <span class="badge badge-secondary">
                <i class="fas fa-calendar-alt"></i>
                <?= ' ' . date("D", strtotime($arr_arg['occurs_at'])) . ' '; ?>
            </span><br>
            <strong>
               <span class="redo-1"> 
                        <?= date("M j", strtotime($arr_arg['occurs_at'])); ?>
               </span>
            </strong><br>
            <span class="time">
                <?= date("g:ia", strtotime($arr_arg['occurs_at'])); ?>
            </span><br>
        </div>
        <div class="col-md-10">
            <h3 id="title-event"><?= $arr_arg['name']; ?></h3>
            <h4 class="gray"><i class="fas fa-map-marker-alt"></i>  <?= $arr_arg['venue']['name'] . ', ' . $arr_arg['venue']['location']; ?></h4>
        </div>
    </div>
    <!--end-->

    <div class="row">
        <div class="col-md-7 font-size-15-px">
            <p class="margin-0 gray"><b><i class="fas fa-archive"></i> Category: </b><a href="<?= $arr_arg['get_url_site'] . '/' . $arr_arg['category_slug'] . '/' . $arr_arg['category']['id'] ?>" class="badge badge-secondary"> <?= $arr_arg['category']['name']; ?></a></p>
            <p class="margin-0 gray"><b><i class="fas fa-hotel"></i> Venue: </b><a href="<?= $arr_arg['get_url_site'] . '/' . $arr_arg['venues_slug'] . '/' . $arr_arg['venue']['id'] ?>" class="badge badge-secondary"> <?= $arr_arg['venue']['name']; ?></a></p>
            <p class="gray"><b><i class="fas fa-user-friends"></i> Performer: </b> 
                <?php foreach ($arr_arg['performances'] as $performer) { ?>
                    <a href="<?= $arr_arg['get_url_site'] . '/' . $arr_arg['performers_slug'] . '/' . $performer['performer']['id'] ?>" class="badge badge-secondary">
                        <?= $performer['performer']['name']; ?>
                    </a>,
                    <?php
                }
                ?> 
            </p>
        </div>

        <!--        cart-->
        <div class="col-md-5 cart-tickets">
            <div class="block-hide-ticket-cart">
                <h4 class="d-flex cours justify-content-between align-items-center mb-3">
                    <span class="text-muted">Your cart <i class="fas fa-shopping-cart"></i></span>
                    <span class="badge badge-secondary badge-pill count-cart">0</span>
                </h4>
            </div>
            <!--    info box for js-->
            <div class="info-box-for-js hide" attr-slug-checkout-page="<?= get_post(get_option('ticket_evolution_option')["page_checkout"])->post_name ?>"></div>
            <!--end-->
        </div>
        <!--end-->

        <br>
    </div>
    
        <!--tickets error-->
        <div id="tickets-error" class="alert alert-danger" role="alert" style="display:none;">
            Sorry, no tickets found
        </div>
        <!--end-->
        
        <div class="content-tickets-block">

            <!--    seatmap-->
            <div class="row listing">
                <div class="col-md-12">
                    <div id='map-<?= $arr_arg['id']; ?>' class="seatmap"></div>
                </div>
            </div> 
            <!--end-->

            <!-- table seatmap -->
            <div class="single_events">
                <table id="dt-material-checkbox" class="table table-striped" cellspacing="0" width="100%">
                    <?php if (!empty($arr_arg['ticket_groups'])) : ?>
                        <?php foreach ($arr_arg['ticket_groups'] as $event) : ?>
                            <tbody>
                                <tr class="shadow p-3 mb-5 bg-white rounded">
                                    <td class="left-tdf">
                                        <div class="tickets-section-row-<?= $event['id'] ?>">
                                            <?= $event['section']; ?><br>
                                            Row: <?= $event['row']; ?>
                                             <span class="sea-ticket-list-quantity-sep"> • </span>
                                            <span class="badge badge-success">
                                                <?= $event['available_quantity']; ?> Tickets
                                            </span>
                                        </div>
                                    </td>
                                    <td>
                                        <div class="tickets-real-price-<?= $event['id'] ?>">
                                            <span id="price-ticket-<?= $event['id'] ?>" attr-price="<?= $event['retail_price'] ?>">
                                            $<?= $event['retail_price'] ?>
                                            </span>
                                        </div>
                                    </td>
                                    <td class="right-tdf">
                                        <button id="<?= $event['id'] ?>" type="button" class="tickets btn btn-dark btn-sm center btn-danger" data-html="true" data-toggle="popover" title="How many tickets?"  data-content="">
                                            Buy <i class="fas fa-caret-right"></i><br>
                                        </button>
                                        <div id="popover-content-<?= $event['id'] ?>" class="hide font-size-19px popover-content" style='display:none;'>
                                            How many tickets? <i class="fas fa-times-circle float-right" attr-id="<?= $event['id'] ?>" id="<?= $event['id'] ?>"></i><br>
                                            <input type="number" min="1" max="<?= $event['available_quantity']; ?>" value="1" class="count-tickets" id="count-tickets-<?= $event['id'] ?>" attr-id="<?= $event['id'] ?>"><br>
                                            <div id="error-real-price" style="display: none">
                                                <span class="text-subtotal" style="color:red;">You cannot buy so many tickets</span><br>
                                            </div>
                                            <div class="real-price-block">
                                                <span class="text-subtotal">Estimated Subtotal </span>
                                                <span class="count-tick gray font-size-15-px" id="count-tick-<?= $event['id'] ?>">1</span>
                                                <span class="gray font-size-15-px">x$<?= $event['retail_price'] ?></span>
                                                <span id="all-price-ticket-<?= $event['id'] ?>" class="all-price-ticket badge badge-warning">$<?= $event['retail_price'] ?></span><br>
                                            </div>
                                            <button type="button" class="btn btn-checkout btn-add-to-cart btn-success" data-id="<?= $event['id'] ?>" attr-count-ticket="1" attr-price-ticket="<?= $event['retail_price'] ?>" attr-price-all-tickets="<?= $event['retail_price'] ?>" attr-ticket-section="<?= $event['section']; ?>" attr-ticket-available-quantity="<?= $event['available_quantity']; ?>" attr-ticket-row="<?= $event['row']; ?>">
                                                Add to Cart <i class="fas fa-cart-arrow-down"></i>
                                            </button>
                                        </div>
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </table>
            </div>
            <!-- end -->
        </div>    
</div>
<script>
    
    var ticketGroups = <?php echo json_encode($arr_arg['ticket_groups']); ?>;
    var data_set = new Array();
    
    if(ticketGroups !== null) {
        ticketGroups.forEach(function (ticket) {
        addElementToArray(data_set, ticket);
        
    });
    console.log(ticketGroups);

    // Create seatmap event
    var seatmap = new Tevomaps({
        venueId: '<?= $arr_arg['venue']['id']; ?>',
        configurationId: '<?= $arr_arg['configuration']['id'] ?>',
        ticketGroups: ticketGroups,
       // mapsDomain: 'https://maps-dev.ticketevolution.com',
        showLegend: true,
        selectedSections: [],
        showControls: true,
        mouseControlEnabled: true,
        onSelection: function (sectionIds) {

            var arr = new Array();
            var arr_data = new Array();
            jQuery('#dt-material-checkbox').remove();
            jQuery('.single_events').append(' <table id="dt-material-checkbox" class="table table-striped" cellspacing="0" width="100%"></div>');
            sectionIds.forEach(function (sectionId) {
                ticketGroups.forEach(function (ticketGroup) {
                    if (sectionId == ticketGroup['tevo_section_name']) {
                        arr.push(ticketGroup);
                    }
                });  
            });          
            arr.forEach(function (ticket) {
               addElementToTable(ticket);
            });
            
//            if(sectionIds == 0) {}
        },
    });
    //end 

    var seatmapApi = seatmap.build('map-<?= $arr_arg['id']; ?>');

    function addElementToArray(data_el, ticket) {
        
    }
    
    function addElementToTable(ticket) {
            
        jQuery('#dt-material-checkbox').append(
            '<tbody><tr><td>'+ ticket.section + '<br>Row: ' + ticket.row + ' <span class="sea-ticket-list-quantity-sep"> • </span><span class="badge badge-success">' + ticket.available_quantity +
            'Tickets</span></td><td><span id="price-ticket-'+ticket.id+'" attr-price="'+ticket.retail_price+'">$'+ticket.retail_price+'</span></td><td><button id="' + ticket.id + '" type="button" class="tickets btn btn-dark btn-sm center btn-danger" data-html="true" data-toggle="popover" title="How many tickets?"  data-content="">' +
            'Buy <i class="fas fa-caret-right"></i><br></button><div id="popover-content-' + ticket.id + '" class="hide font-size-19px" style="display:none;">' +
            ' How many tickets? <i class="fas fa-times-circle float-right" attr-id="' + ticket.id + '" id="' + ticket.id + '"></i><br>' +
            '<input type="number" min="1" max="' + ticket.available_quantity + '" value="1" class="count-tickets" id="count-tickets-' + ticket.id + '" attr-id="' + ticket.id + '"><br>' +
            '<div id="error-real-price" style="display: none"><span class="text-subtotal" style="color:red;">You cannot buy so many tickets</span><br></div><div class="real-price-block"><span class="text-subtotal">Estimated Subtotal </span><span class="count-tick gray font-size-15-px" id="count-tick-' + ticket.id + '">1</span>' +
            '<span class="gray font-size-15-px">x$' + ticket.retail_price + '</span> <span id="all-price-ticket-' + ticket.id + '" class="all-price-ticket badge badge-warning">$' + ticket.retail_price +'</span><br></div>' +
            ' <div class="button-add-cart-single"><button type="button" class="btn btn-checkout btn-add-to-cart btn-success" data-id="' + ticket.id + '" attr-count-ticket="1" attr-price-ticket="' + ticket.retail_price + '" attr-price-all-tickets="' + ticket.retail_price + '" attr-ticket-section="' + ticket.section + '" attr-ticket-available-quantity="' + ticket.available_quantity + '" attr-ticket-row="' + ticket.row + '">' +
            'Add to Cart <i class="fas fa-cart-arrow-down"></i></button></div></td></tr> </tbody>'
            );
    
        jQuery('.btn.tickets').on('click', function () { 
            jQuery('#' + this.id).hide();
            jQuery('#popover-content-' + this.id).show();
        });
        
        jQuery('.fas').on('click', function () { 
            jQuery('#' + this.id).show();
            jQuery('#popover-content-' + this.id).hide();             
        });
        
        jQuery('.button-add-cart-single').on('click', function () { 
            jQuery.toast({
                heading: 'Successfully',
                text: 'Added to cart!',
                showHideTransition: 'slide',
                icon: 'success',
                position: 'top-right',
                loaderBg: '#28a745',
                loader: true,
                hideAfter: 5000
            });
        });
        
         jQuery("input.count-tickets").bind("change paste keyup", function () {
            var attr_id = jQuery(this).attr('attr-id');
            var attr_price = parseFloat(jQuery('#price-ticket-' + attr_id).attr('attr-price'));
            jQuery('span#count-tick-' + attr_id).html(jQuery(this).val());
            jQuery('#all-price-ticket-' + attr_id).html('$' + (attr_price * parseInt(jQuery(this).val())));
            jQuery('button[data-id=' + attr_id + ']').attr('attr-count-ticket', jQuery(this).val());
            jQuery('button[data-id=' + attr_id + ']').attr('attr-price-all-tickets', attr_price * parseInt(jQuery(this).val()));
            
            if(parseInt(jQuery(this).val()) > jQuery('button[data-id=' + attr_id + ']').attr('attr-ticket-available-quantity')) {
                jQuery('button[data-id=' + attr_id + ']').attr('disabled', true);
                jQuery('.real-price-block').hide();
                jQuery('#error-real-price').show();
            } else {
                jQuery('.real-price-block').show();
                jQuery('#error-real-price').hide();
                jQuery('button[data-id=' + attr_id + ']').attr('disabled', false);
            }
        });
        
        jQuery('.btn-add-to-cart').on('click', function () {
            var id = jQuery(this).attr('data-id');
            var count_ticket = jQuery(this).attr('attr-count-ticket');
            var price_ticket = jQuery(this).attr('attr-price-ticket');
            var price_all_tickets = jQuery(this).attr('attr-price-all-tickets');
            var ticket_section = jQuery(this).attr('attr-ticket-section');
            var ticket_row = jQuery(this).attr('attr-ticket-row');
            var title_event = jQuery('.tickevolution-event .title-event #title-event').text();
            var k = 0;
            var arr_tickets_user = [
                {
                    'id': id,
                    'count_ticket': count_ticket,
                    'price_all_tickets': price_all_tickets,
                    'price_ticket': price_ticket,
                    'ticket_section': ticket_section,
                    'ticket_row': ticket_row,
                    'title_event': title_event
                }
            ];
            if (typeof jQuery.cookie('tickets_user') == 'undefined') {
                jQuery.cookie("tickets_user", JSON.stringify(arr_tickets_user), {path: '/'});
            } else {

                var coockie_tikets = jQuery.parseJSON(jQuery.cookie("tickets_user"));

                coockie_tikets.forEach(function (ticket) {
                    if (ticket.id == id) {
                        //console.log('true' + k);
                        coockie_tikets.splice(k, 1);
                        k = 0;
                    }
                    k++;
                });
                coockie_tikets.push({
                    'id': id,
                    'count_ticket': count_ticket,
                    'price_all_tickets': price_all_tickets,
                    'price_ticket': price_ticket,
                    'ticket_section': ticket_section,
                    'ticket_row': ticket_row,
                    'title_event': title_event
                });
                jQuery.cookie("tickets_user", JSON.stringify(coockie_tikets), {path: '/'});
            }
            console.log(jQuery.parseJSON(jQuery.cookie("tickets_user")));

            //add to cart
            createCart();
        });
        
        function createCart() {
            jQuery('.tickevolution-event .count-cart').text(jQuery.parseJSON(jQuery.cookie("tickets_user")).length);
            jQuery('.tickevolution-event .list-tikets').remove();

            var cookie_tickets_arr = jQuery.parseJSON(jQuery.cookie("tickets_user"));
            var total_price = 0;

            jQuery('.tickevolution-event .cart-tickets').append('<ul class="list-group mb-3 list-tikets" style="display:none">');

            cookie_tickets_arr.forEach(function (ticket) {
                jQuery('.tickevolution-event .list-tikets').append('<li class="list-group-item justify-content-between lh-condensed" attr-id="' + ticket.id + '"><div>');
                jQuery('.tickevolution-event ul.list-tikets li[attr-id="' + ticket.id + '"] div').append('<h6 class="title-event">' + ticket.title_event + '</h6>');
                jQuery('.tickevolution-event ul.list-tikets li[attr-id="' + ticket.id + '"] div').append('<small class="text-muted">Section: ' + ticket.ticket_section + '</small><br>');
                jQuery('.tickevolution-event ul.list-tikets li[attr-id="' + ticket.id + '"] div').append('<small class="text-muted">Row: ' + ticket.ticket_row + '</small><br>');
                jQuery('.tickevolution-event ul.list-tikets li[attr-id="' + ticket.id + '"] div').append('<small class="text-muted"><span class="badge badge-success">' + ticket.count_ticket + ' Tickets </span></small><span class="text-muted price-all-tikets float-right">$' + ticket.price_all_tickets + '</span>');
                jQuery('.tickevolution-event .list-tikets').append('</div>');
                jQuery('.tickevolution-event .list-tikets').append('</li>');

                total_price = total_price + parseFloat(ticket.price_all_tickets);

            });

            jQuery('.tickevolution-event .list-tikets').append('<li class="list-group-item d-flex justify-content-between lh-condensed" attr-total="total"><div>');
            jQuery('.tickevolution-event .list-tikets li[attr-total=total] div').append('Total:<strong><span class="float-right all-price-in-cart"> $' + total_price + '</span></strong>');
            jQuery('.tickevolution-event .list-tikets').append('</div></li>');

            jQuery('.tickevolution-event .cart-tickets').append('</ul">');
            jQuery('.tickevolution-event .list-tikets').append('<a class="btn btn-secondary btn-cart-to-chekout" href="/'+ jQuery('.tickevolution-event .cart-tickets .info-box-for-js').attr('attr-slug-checkout-page')+'" role="button">Go to Secure Checkout</a>');
        }
        
        }
    } else {
       jQuery('#tickets-error').show();
       jQuery('.single_events').hide();
       jQuery('.seatmap').hide();
    }
</script>