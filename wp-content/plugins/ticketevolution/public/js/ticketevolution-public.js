(function ($) {
    'use strict';
    jQuery(window).ready(function () {
        
        //Title pages 
        if(jQuery("div").is(".tickevolution-event")) jQuery('title').html(jQuery('#title-event').text()); //tickevolution-event
        if(jQuery("div").is(".tickevolution.listing"))jQuery('title').html(jQuery('.entry-title').text()); //listing: performers/events
        if(jQuery("div").is(".ticketevolution-checkout")) jQuery('title').html(jQuery('.entry-title').text()); //ticketevolution-checkout
            
            window.addEventListener('load', function () {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function (form) {
                form.addEventListener('submit', function (event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);

        //On single event page

        jQuery('ul.list-tikets').hide();    

        if (typeof jQuery.cookie('tickets_user') != 'undefined') {
            createCart();
        }
        ;

        jQuery('.btn-cart-to-chekout').on('click', function () {
            window.location.href = document.location.origin + '/' + jQuery('.cart-tickets .info-box-for-js').attr('attr-slug-checkout-page');
        });

        jQuery('.btn.tickets').on('click', function () { 
            jQuery('#' + this.id).hide();
            jQuery('#popover-content-' + this.id).show();
            if(window.matchMedia('(max-width: 600px)').matches) {
                jQuery('.tickets-section-row-' + this.id).hide();
                jQuery('.tickets-real-price-' + this.id).hide();
            }
        });
        
         jQuery('.fas').on('click', function () { 
            jQuery('#' + this.id).show();
            jQuery('#popover-content-' + this.id).hide(); 
            jQuery('.tickets-section-row-' + this.id).show();
            jQuery('.tickets-real-price-' + this.id).show();
        });
        
        jQuery('.btn-checkout').on('click', function () { 
            jQuery.toast({
                heading: 'Successfully',
                text: 'Added to cart!',
                showHideTransition: 'slide',
                icon: 'success',
                position: 'top-right',
                loaderBg: '#28a745',
                loader: true,
                hideAfter: 5000
            });
        });
        
        jQuery('.btn-add-to-cart').on('click', function () {
            var id = jQuery(this).attr('data-id');
            var count_ticket = jQuery(this).attr('attr-count-ticket');
            var price_ticket = jQuery(this).attr('attr-price-ticket');
            var price_all_tickets = jQuery(this).attr('attr-price-all-tickets');
            var ticket_section = jQuery(this).attr('attr-ticket-section');
            var ticket_row = jQuery(this).attr('attr-ticket-row');
            var title_event = jQuery('.title-event #title-event').text();
            var link_ticket = window.location.href;
            var link_page_checkout = jQuery('.cart-tickets .info-box-for-js').attr('attr-slug-checkout-page');
            var k = 0;
            var arr_tickets_user = [
                {
                    'id': id,
                    'count_ticket': count_ticket,
                    'price_all_tickets': price_all_tickets,
                    'price_ticket': price_ticket,
                    'ticket_section': ticket_section,
                    'ticket_row': ticket_row,
                    'title_event': title_event,
                    'link_ticket': link_ticket,
                    'link_page_checkout': link_page_checkout,
                }
            ];
            if (typeof jQuery.cookie('tickets_user') == 'undefined') {
                jQuery.cookie("tickets_user", JSON.stringify(arr_tickets_user), {path: '/'});
            } else {

                var coockie_tikets = jQuery.parseJSON(jQuery.cookie("tickets_user"));

                coockie_tikets.forEach(function (ticket) {
                    if (ticket.id == id) {
                            //console.log('true' + k);
                        coockie_tikets.splice(k, 1);
                        k = 0;
                    }
                    k++;
                });
                coockie_tikets.push({
                    'id': id,
                    'count_ticket': count_ticket,
                    'price_all_tickets': price_all_tickets,
                    'price_ticket': price_ticket,
                    'ticket_section': ticket_section,
                    'ticket_row': ticket_row,
                    'title_event': title_event,
                    'link_ticket': link_ticket,
                    'link_page_checkout': link_page_checkout
                });
                jQuery.cookie("tickets_user", JSON.stringify(coockie_tikets), {path: '/'});
            }
            console.log(jQuery.parseJSON(jQuery.cookie("tickets_user")));

            //add to cart
            createCart();
        });
        
        if (!jQuery("div").is(".tickevolution-cart-shortcode")) {
                jQuery('.block-hide-ticket-cart').on('click', function () {
                    jQuery('ul.list-tikets').toggle();
            });
        }

        jQuery('.fa-times-circle').on('click', function () {
            jQuery('#popover-content-' + jQuery(this).attr('attr-id')).hide();
            jQuery('button#' + jQuery(this).attr('attr-id')).show();
        });

        jQuery("input.count-tickets").bind("change paste keyup", function () {
            var attr_id = jQuery(this).attr('attr-id');
            var attr_price = parseFloat(jQuery('#price-ticket-' + attr_id).attr('attr-price'));
            jQuery('span#count-tick-' + attr_id).html(jQuery(this).val());
            jQuery('#all-price-ticket-' + attr_id).html('$' + (attr_price * parseInt(jQuery(this).val())));
            jQuery('button[data-id=' + attr_id + ']').attr('attr-count-ticket', jQuery(this).val());
            jQuery('button[data-id=' + attr_id + ']').attr('attr-price-all-tickets', attr_price * parseInt(jQuery(this).val()));
            
            if (parseInt(jQuery(this).val()) > jQuery('button[data-id=' + attr_id + ']').attr('attr-ticket-available-quantity')) {
                jQuery('button[data-id=' + attr_id + ']').attr('disabled', true);
                jQuery('.real-price-block').hide();
                jQuery('#error-real-price').show();
            } else {
                jQuery('.real-price-block').show();
                jQuery('#error-real-price').hide();
                jQuery('button[data-id=' + attr_id + ']').attr('disabled', false);
            }
        });
               
        function createCart() {
            jQuery('.count-cart').text(jQuery.parseJSON(jQuery.cookie("tickets_user")).length);
            jQuery('.list-tikets').remove();

            var cookie_tickets_arr = jQuery.parseJSON(jQuery.cookie("tickets_user"));
            var total_price = 0;
            var link_page_checkout;

            jQuery('.cart-tickets').append('<ul class="list-group mb-3 list-tikets" style="display:none">');
            jQuery('.cart-tickets-checkout').append('<ul class="list-group mb-3 list-tikets">');
            jQuery('#list-my-cart').append('<ul class="list-group mb-3 list-tikets">');
                        
            if (jQuery("div").is(".tickevolution-cart-shortcode")) {
                jQuery('.tickevolution-cart-shortcode').append('<ul class="list-group mb-3 list-tikets">');
            }

            cookie_tickets_arr.forEach(function (ticket) {
                jQuery('.list-tikets').append('<li class="list-group-item justify-content-between lh-condensed" attr-id="' + ticket.id + '"><div>');
                jQuery('ul.list-tikets li[attr-id="' + ticket.id + '"] div').append('<i class=" fa-sm close fas fa-times-circle float-right" attr-id="' + ticket.id + '"></i>');
                jQuery('ul.list-tikets li[attr-id="' + ticket.id + '"] div').append('<h6 class="title-event"><a href="' + ticket.link_ticket + '" class="text-dark">' + ticket.title_event + '</a></h6>');                
                jQuery('ul.list-tikets li[attr-id="' + ticket.id + '"] div').append('<small class="text-muted">Section: ' + ticket.ticket_section + '</small><br>');
                jQuery('ul.list-tikets li[attr-id="' + ticket.id + '"] div').append('<small class="text-muted">Row: ' + ticket.ticket_row + '</small><br>');
                jQuery('ul.list-tikets li[attr-id="' + ticket.id + '"] div').append('<small class="text-muted"><span class="badge badge-success">' + ticket.count_ticket + ' Tickets </span></small><span class="text-muted price-all-tikets float-right">$' + ticket.price_all_tickets + '</span>');
                jQuery('.list-tikets').append('</div>');
                jQuery('.list-tikets').append('</li>');
                                
                total_price = total_price + parseFloat(ticket.price_all_tickets);
                link_page_checkout = ticket.link_page_checkout;                
            });
            
            jQuery('.list-tikets').append('<div class="promo-code-form"><form method="POST"><input type="text" id="promo-input" name="promo-code" placeholder="Promo"><button name=promo-button>Activate</button></form></div>');
            
            jQuery('.list-tikets').append('<li class="list-group-item d-flex justify-content-between lh-condensed" attr-total="total"><div>');
            jQuery('.list-tikets li[attr-total=total] div').append('Total:<strong><span class="float-right all-price-in-cart"> $' + total_price + '</span></strong>');
            jQuery('.list-tikets').append('</div></li>');

            jQuery('.cart-tickets').append('</ul">');
                                   
            if(jQuery("div").is(".show_btn_redirect_checkout")) {
                jQuery('.list-tikets').append('<a class="btn btn-secondary btn-cart-to-chekout" href="/'+ link_page_checkout +'" role="button">Go to Secure Checkout</a>');
            }
            
            if(!jQuery("div").is("#shortcode-cart")) {
                jQuery('.list-tikets').append('<a class="btn btn-secondary btn-cart-to-chekout" href="/'+ link_page_checkout +'" role="button">Go to Secure Checkout</a>');
            }
        }    
                
        //Click on button Close on checkout page in cart        
        jQuery('ul.list-tikets i.close').on('click', function () {
            var id = jQuery(this).attr('attr-id');
            var coockie_tikets = jQuery.parseJSON(jQuery.cookie("tickets_user"));
            var k = 0;
            var total_price = 0;

            jQuery('ul.list-tikets li[attr-id="' + id + '"]').hide("slow");

            coockie_tikets.forEach(function (ticket) {
                if (ticket.id == id) {
                    coockie_tikets.splice(k, 1);
                    k = 0;
                }
                k++;

            });

            coockie_tikets.forEach(function (ticket) {
                total_price = total_price + parseFloat(ticket.price_all_tickets);
            });

            jQuery('.cart-tickets .count-cart').text(coockie_tikets.length);
            jQuery('.all-price-in-cart').text('$' + total_price);
            jQuery.cookie("tickets_user", JSON.stringify(coockie_tikets), {path: '/'});

            jQuery.toast({
                heading: 'Successfully',
                text: 'Removed from the basket!',
                showHideTransition: 'slide',
                icon: 'success',
                position: 'top-right',
                loader: true,
                hideAfter: 3000
            });

            //console.log(jQuery.parseJSON(jQuery.cookie("tickets_user")));
        });
        //end
        
        if (jQuery('.count-cart').text() == '0') {
            jQuery('.btn-cart-to-chekout').attr("disabled", true);
        }
        //end checkout page        
    });

})(jQuery); 
