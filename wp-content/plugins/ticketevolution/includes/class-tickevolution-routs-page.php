<?php

/**
 * Class Routes for page Events, Performers, Venues, Checkout
 * 
 * @property object GetApi() Get class GetApi,
 * @property int get_setting_page_listing_events_id() Get Setting "page_listing_events" with page settings Ticket Evolution Option ,
 * @property object TicketevolutionPublicDisplay() Get class TicketevolutionPublicDisplay,
 * @property object template_listing_events_page() Generation listing events page,
 */
class RoutesPage {

    private $true_event;
    private $true_performers;

    public function __construct() {
        $this->template_listing_events_page();
        $this->template_listing_performers_page();
        $this->template_listing_venues_page();
        $this->template_category_page();
        $this->template_checkout_page();
        $this->template_login_page();
        $this->template_user_profile();
    }

    /**
     * 
     * @return \GetApi
     */
    public function GetApi() {
        return new GetApi();
    }

    /**
     * 
     * @return \PostApi
     */
    public function PostApi() {
        return new PostApi();
    }

    public function stdClass() {
        return new stdClass();
    }

    /**
     * 
     * @return \TicketevolutionPublicDisplay
     */
    public function TicketevolutionPublicDisplay() {
        return new TicketevolutionPublicDisplay();
    }

    /**
     * This function return setting page listing id
     * 
     * @param type $page
     * @return type
     */
    public function get_setting_page_listing_id($page) {
        return get_option('ticket_evolution_'.$page.'_option')["page_listing_$page"];
    }

    public function update_emails_client($arr_info_client, $client_id_in_api) {
        return $this->TicketevolutionPublicDisplay()->update_email($arr_info_client, $client_id_in_api);
    }

    public function update_telephone_client($arr_info_client, $client_id_in_api) {
        return $this->TicketevolutionPublicDisplay()->update_telephone($arr_info_client, $client_id_in_api);
    }

    public function create_credit_card($arr_info_client, $client_id_in_api) {
        return $this->TicketevolutionPublicDisplay()->create_credit($arr_info_client, $client_id_in_api);
    }

    public function update_addresses_client($arr_info_client, $client_id_in_api) {
        return $this->TicketevolutionPublicDisplay()->update_addresses($arr_info_client, $client_id_in_api);
    }

    /**
     * This function return total entries (count events) in Api Tickevolution
     * 
     * @return type int
     */
   public function count_events() {
       return (int) $this->GetApi()->arr_arg_for_events_listing()['total_entries'];
   }

    /**
     * This function return total entries (count performers) in Api Tickevolution
     * 
     * @return type int
     */
    public function count_performers() {
        return (int) $this->GetApi()->arr_arg_for_performers_listing()['total_entries'];
    }

    /**
     * This function return total entries (count venues) in Api Tickevolution
     * 
     * @return type int
     */
    public function count_venues() {
        return (int) $this->GetApi()->arr_arg_for_venues_listing()['total_entries'];
    }

    public function template_listing_events_page() {

        add_filter('the_content', function($text) {
            
            echo $text;

            if ($this->get_setting_page_listing_id('events') == get_the_ID() && !is_admin()) {

                if ($this->count_events() != 0) {

                    $arr_events = $this->GetApi()->arr_arg_for_events_listing($this->TicketevolutionPublicDisplay()::current_page(), $this->TicketevolutionPublicDisplay()->get_number_pre_page('events'), 'events.occurs_at ASC');
                    $arr_events['arr_for_pagination']['current_page'] = $this->TicketevolutionPublicDisplay()::current_page();

                    if ($this->TicketevolutionPublicDisplay()->get_id_with_url() != 'this_pagination') {

                        $event = $this->GetApi()->get_search_by_id('event', $this->TicketevolutionPublicDisplay()->get_id_with_url());                       
                        $this->TicketevolutionPublicDisplay()->template('events/single/content', $event);
                    } else {
                        $this->TicketevolutionPublicDisplay()->template('events/listing-events-page', $arr_events);
                        $this->TicketevolutionPublicDisplay()->template('pagination', $arr_events['arr_for_pagination']);
                    }
                } else {
                    $this->TicketevolutionPublicDisplay()->template('events/not-event-listing', $arr_events);
                }
            }          
        });
    }

    public function template_listing_performers_page() {

        add_filter('the_content', function($text) {

            if ($this->get_setting_page_listing_id('performers') == get_the_ID() && !is_admin()) {

                if ($this->count_performers() != 0) {

                    $arr_performers = $this->GetApi()->arr_arg_for_performers_listing($this->TicketevolutionPublicDisplay()::current_page(), $this->TicketevolutionPublicDisplay()->get_number_pre_page('performers'));
                    $arr_performers['arr_for_pagination']['current_page'] = $this->TicketevolutionPublicDisplay()::current_page();

                    if ($this->TicketevolutionPublicDisplay()->get_id_with_url() != 'this_pagination') {

                        $performer = $this->GetApi()->get_search_by_id('performer', $this->TicketevolutionPublicDisplay()->get_id_with_url());

                        $performer['events'] = $this->GetApi()->get_search_list_events(1, 100, '', $performer['id']);
                        $performer['get_url_site'] = $arr_performers['get_url_site'];
                        $performer['get_slug_listing_event_page'] = $arr_performers['get_slug_listing_event_page'];


                        $this->TicketevolutionPublicDisplay()->template('performers/single/content', $performer);
                    } else {

                        $this->TicketevolutionPublicDisplay()->template('performers/listing-performers-page', $arr_performers);
                        $this->TicketevolutionPublicDisplay()->template('pagination', $arr_performers['arr_for_pagination']);
                    }
                } else {
                    $this->TicketevolutionPublicDisplay()->template('performers/not-performers-listing', $arr_performers);
                }
            }
        });
    }

    public function template_listing_venues_page() {

        add_filter('the_content', function($text) {

            if ($this->get_setting_page_listing_id('venues') == get_the_ID() && !is_admin()) {

                if ($this->count_venues() != 0) {

                    $arr_venues = $this->GetApi()->arr_arg_for_venues_listing($this->TicketevolutionPublicDisplay()::current_page(), $this->TicketevolutionPublicDisplay()->get_number_pre_page('venues'));
                    $arr_venues['arr_for_pagination']['current_page'] = $this->TicketevolutionPublicDisplay()::current_page();

                    if ($this->TicketevolutionPublicDisplay()->get_id_with_url() != 'this_pagination') {

                        $venue = $this->GetApi()->get_search_by_id('venue', $this->TicketevolutionPublicDisplay()->get_id_with_url());

                        $venue['events'] = $this->GetApi()->get_search_list_events_by_venue_id(1, 100, '', $venue['id']);
                        $venue['get_url_site'] = $arr_venues['get_url_site'];
                        $venue['get_slug_listing_event_page'] = $arr_venues['get_slug_listing_event_page'];

                        $this->TicketevolutionPublicDisplay()->template('venues/single/content', $venue);
                    } else {

                        $this->TicketevolutionPublicDisplay()->template('venues/listing-venues-page', $arr_venues);
                        $this->TicketevolutionPublicDisplay()->template('pagination', $arr_venues['arr_for_pagination']);
                    }
                } else {
                    $this->TicketevolutionPublicDisplay()->template('venues/not-venues-listing', $arr_venues);
                }
            }
        });
    }

    public function template_category_page() {

        add_filter('the_content', function($text) {
            if (get_option('ticket_evolution_events_option')['page_category_events'] == get_the_ID() && !is_admin()) {

                if ($this->TicketevolutionPublicDisplay()->get_id_with_url() >= 1) {

                    $events = $this->GetApi()->get_search_event_by_category_id(1, 100, $this->TicketevolutionPublicDisplay()->get_id_with_url())['events'];

                    $events['get_url_site'] = $this->TicketevolutionPublicDisplay()->get_url_site();
                    $events['get_slug_listing_event_page'] = $this->TicketevolutionPublicDisplay()->get_slug_listing_page('events');

                    $this->TicketevolutionPublicDisplay()->template('category/category-page', $events);
                } else {
                    
                }
            }
        });
    }

    public function template_checkout_page() {

        add_filter('the_content', function($text) {
            if (get_option('ticket_evolution_option')['page_checkout'] == get_the_ID() && !is_admin()) {

                $cookies = $this->GetApi()::get_cookies();

                //var_dump($this->GetApi()->get_promotion_code());
                echo '<pre>';
//                print_r($_POST['checkout-form']); // checkout form
                //var_dump($this->GetApi()->get_list_orders());
                echo '</pre>';

                //var_dump($this->PostApi()->create_orders());

                foreach ($cookies as $key => $value) {

                    if ($value['id'] != 0 || $value['id']) {
                        $value['info_ticket_group'] = $this->GetApi()->get_ticket_groups($value['id']);
                        $value['list_service_fee'] = $this->GetApi()->get_list_service_fee();
                        $value['delivery_options'] = $this->GetApi()->get_delivery_option();
                        $value['promotion_code'] = $this->GetApi()->get_promotion_code('example_promo_code');
                        $cookies_arr[] = $value;
                    }

                    if (!$_COOKIE['tickets_user']) {
                        $cookies_arr['tickets_user'] = false;
                    } else {
                        $cookies_arr['tickets_user'] = true;
                    }
                }

                $cookies_arr['items_for_tab_cart'] = $this->TicketevolutionPublicDisplay()->render_items_li_for_cart($cookies);
                $cookies_arr['total_price_for_tab_cart'] = $this->TicketevolutionPublicDisplay()->total_price_for_cart($cookies);

                $this->TicketevolutionPublicDisplay()->template('checkout/content', $cookies_arr);
            }
        });
    }

    public function template_login_page() {

        add_filter('the_content', function($text) {
            if (get_option('ticket_evolution_option')['page_login_users'] == get_the_ID() && !is_admin()) {

                $arr_arg = wp_login_form($this->GetApi()::arg_for_login_form());
                $this->TicketevolutionPublicDisplay()->template('login', $arr_arg);
            }
        });
    }

    public function template_user_profile() {
        add_filter('the_content', function($text) {
            if ($this->TicketevolutionPublicDisplay()->check_user_page() && !is_admin() && is_user_logged_in()) {

                $client_id_in_api = (int) $this->TicketevolutionPublicDisplay()->get_id_with_url();
                
                if (!empty($this->GetApi()->get_client_info($client_id_in_api))) {
                    $arr_info_client = $this->GetApi()->get_client_info($client_id_in_api);
                    
                    //update profile
                    $this->update_emails_client($arr_info_client, $client_id_in_api);
                    $this->update_telephone_client($arr_info_client, $client_id_in_api);
                    $this->update_addresses_client($arr_info_client, $client_id_in_api);
                    $this->create_credit_card($arr_info_client, $client_id_in_api);
                    
                    $arr_info_client['cookies'] = $this->GetApi()::get_cookies();
                    $arr_info_client['items_for_tab_cart'] = $this->TicketevolutionPublicDisplay()->render_items_li_for_cart($arr_info_client['cookies']);
                    $arr_info_client['total_price_for_tab_cart'] = $this->TicketevolutionPublicDisplay()->total_price_for_cart($arr_info_client['cookies']);
                    $arr_info_client['credit_cards'] = $this->GetApi()->get_client_credite_cards($client_id_in_api)['credit_cards'];

                    if ($arr_info_client['primary_shipping_address']['region']) {
                        $arr_info_client['show_danger'] = $this->TicketevolutionPublicDisplay()->show_danger_credit_cart($arr_info_client['primary_shipping_address']['primary']);
                    }

                    //$arr_info_client['ticket'] = $this->GetApi()->get_show_tickets(257489392);
//                $order = $this->GetApi()->get_std_сlass();
//                
//                $order_ticket_group = $this->PostApi()->get_std_сlass();
//                $order_ticket_group->id = 324466901;
//                $order_ticket_group->price = 145.48;
//                $order_ticket_group->quantity = 2;
//                $order->ticket_group = [$order_ticket_group];
//                
//                $order_payments = $this->PostApi()->get_std_сlass();
//                $order_payments->type = 'credit_card';
//                $order_payments->credit_card_id = 327284;
//                $order_payments->amount = 305.96;
//                $order->payments = [$order_payments];
//                
//                $order_delivery = $this->PostApi()->get_std_сlass();
//                $order_delivery->type = 'Eticket';
//                $order_delivery->email_address_id = 1965046;
//                $order_delivery->cost = 5.00;
//                $order->delivery = [$order_delivery];
//                
//                $order->client_id = 1507094;
//                $order->service_fee = 10.00;
//                $order->discount = 5.00;
//                $order->promo_code = 'TAKE5';
//                
//                echo '<pre>';
//                   // print_r($order);
//                echo '</pre>';
                    //$this->PostApi()->create_orders($order);
                    $this->TicketevolutionPublicDisplay()->template('users/content', $arr_info_client);
                    $this->TicketevolutionPublicDisplay()->template('users/tabs', $arr_info_client);
                }
            }
        });
    }

}

$RoutsPage = new RoutesPage();

