<div class="tickevolution listing">
    <div class="row">
        <div class="col-md-12 list">
            <div class="items">
                <?php
                foreach ($arr_arg['venues'] as $venues) {
                    ?>
                    <div class="row title-event alert alert-secondary"
                         attr-times="<?= date("A", strtotime($event['occurs_at'])); ?>"
                         attr-months="<?= date("F", strtotime($event['occurs_at'])); ?>"
                         attr-days="<?= date("l", strtotime($event['occurs_at'])); ?>"
                         attr-vanues="<?= $event['venue']['name']; ?>">
                        <div class="col-md-9">
                            <h4 id="title-event"><?= $venues['name'] ?></h4>
                        </div>
                        <div class="col-md-3 float-right">
                            <a href="<?= $arr_arg['get_url_site'] . '/' . $arr_arg['get_slug_listing_venues_page'] . '/' . $venues['id'] . '/'; ?>"class="btn btn-success float-right" role="button" aria-pressed="true">View Events <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <?php
                }
                ?>  
            </div>
        </div>
    </div>
</div>

