<?php

class TicketEvolution_Shortcode {

    protected function TicketEvolutionPublicDisplay() {
        return new TicketEvolutionPublicDisplay();
    }
    
    protected function GetApi() {
        return new GetApi();
    }
    
    public function get_setting_page_listing_id($page) {
        return get_option('ticket_evolution_'.$page.'_option')["page_listing_$page"];
    }

    public function __construct() {
        add_shortcode('events', array($this, 'events'));
        add_shortcode('venues', array($this, 'venues'));
        add_shortcode('performers', array($this, 'performers'));//[performers]
        add_shortcode('cart', array($this, 'cart'));
        add_shortcode('event_widget', array($this, 'event_widget'));
        add_shortcode('ticketevolution-register-form', array($this, 'register_form'));
        add_shortcode('ticketevolution-login-form', array($this, 'login_form'));
    }

    public function events($atts) {

        $params = shortcode_atts([
            'number' => 100,
            'venue_id' => '',
            'office_id' => '',
            'performer_id' => '',
            'category_id' => '',
            'by_time' => '',
//            'only_with_tickets' => ''
                ], $atts);

        $arr['page'] = 1;
        $arr['per_page'] = (int) $params['number'];

        if ($params['venue_id'] != '') {
            $arr['venue_id'] = (int)$params['venue_id'];
        }
        if ($params['office_id'] != '') {
            $arr['office_id'] = (int)$params['office_id'];
        }
        if ($params['performer_id'] != '') {
            $arr['performer_id'] = (int)$params['performer_id'];
        }
        if ($params['category_id'] != '') {
            $arr['category_id'] = (int)$params['category_id'];
        }
        if ($params['by_time'] != '') {
            $arr['by_time'] = $params['by_time'];
        }
//        if ($params['only_with_tickets'] != '') {
//            $arr['only_with_tickets'] = $params['only_with_tickets'];
//        }

        $arr_events = $this->GetApi()->GetApi()->connectedApiTicket()->listEvents($arr);
        $arr_events['get_url_site'] = $this->TicketevolutionPublicDisplay()->get_url_site();
        $arr_events['get_slug_listing_event_page'] = $this->TicketevolutionPublicDisplay()->get_slug_listing_page('events');
        
        if($arr_events['total_entries'] != 0){
            $this->TicketEvolutionPublicDisplay()->template('shortcode/events/listing-events-block', $arr_events);
        } else {
            echo '<div class="alert alert-danger" role="alert">Not Found</div>';
        }
        
    }
    
    public function venues($atts) {

        $params = shortcode_atts([
            'number_venues' => 100,
            'name' => '',
            'first_letter' => '',
            'order_by' => '',
                ], $atts);

        $arr['page'] = 1;
        $arr['per_page'] = (int) $params['number_venues'];

        if ($params['name'] != '') {
            $arr['name'] = $params['name'];
        }
        if ($params['first_letter'] != '') {
            $arr['first_letter'] = $params['first_letter'];
        }
        if ($params['order_by'] != '') {
            $arr['order_by'] = $params['order_by'];
        }

        $arr_venues = $this->GetApi()->GetApi()->connectedApiTicket()->listVenues($arr);
        $arr_venues['get_url_site'] = $this->TicketevolutionPublicDisplay()->get_url_site();
        $arr_venues['get_slug_listing_venues_page'] = $this->TicketevolutionPublicDisplay()->get_slug_listing_page('venues');
        
        if($arr_venues['total_entries'] != 0){
            $this->TicketEvolutionPublicDisplay()->template('shortcode/venues/listing-venues-block', $arr_venues);
        } else {
            echo '<div class="alert alert-danger" role="alert">Not Found</div>';
        }
        
    }
    
    public function performers($atts) {

        $params = shortcode_atts([
            'number_performers' => 100,
            'name' => '',
            'first_letter' => '',
            'order_by' => '',
            'venue_id' => '',
            'category_id' => '',
            'only_with_upcoming_events' => ''
                ], $atts);

        $arr['page'] = 1;
        $arr['per_page'] = (int) $params['number_performers'];

        if ($params['name'] != '') {
            $arr['name'] = $params['name'];
        }
        if ($params['first_letter'] != '') {
            $arr['first_letter'] = $params['first_letter'];
        }
        if ($params['order_by'] != '') {
            $arr['order_by'] = $params['order_by'];
        }
        if ($params['venue_id'] != '') {
            $arr['venue_id'] = $params['venue_id'];
        }
        if ($params['category_id'] != '') {
            $arr['category_id'] = $params['category_id'];
        }
        if ($params['only_with_upcoming_events'] != '') {
            $arr['only_with_upcoming_events'] = $params['only_with_upcoming_events'];
        }

        $arr_performers = $this->GetApi()->GetApi()->connectedApiTicket()->listPerformers($arr);
        $arr_performers['get_url_site'] = $this->TicketevolutionPublicDisplay()->get_url_site();
        $arr_performers['get_slug_listing_performers_page'] = $this->TicketevolutionPublicDisplay()->get_slug_listing_page('performers');
        
        if($arr_performers['total_entries'] != 0){
            $this->TicketEvolutionPublicDisplay()->template('shortcode/performers/listing-performers-block', $arr_performers);
        } else {
            echo '<div class="alert alert-danger" role="alert">Not Found</div>';
        }
        
    }
    
    public function cart($param) {

        $params = shortcode_atts(
                [
                    'fixed' => true,
                    'show_btn_redirect_checkout' => true,
                ], $param);

        // fixed cart
        if ($params['fixed'] != false) {
            $arr_performers['fixed'] = 'tickevolution-cart-shortcode';
        } else {
            $arr_performers['fixed'] = null;
        }

        // show button
        if ($params['show_btn_redirect_checkout'] != false) {
            $arr_performers['show_checkout'] = 'show_btn_redirect_checkout';
        } else {
            $arr_performers['show_checkout'] = null;
        }

        $arr_performers['link_page_checkout'] = get_post(get_option('ticket_evolution_option')["page_checkout"])->post_name;
        $this->TicketEvolutionPublicDisplay()->template('shortcode/cart/cart-content', $arr_performers);
    }
    
    public function event_widget($param) {
          
        $arr_performers = $this->GetApi()->get_search_by_id('event', $param['id']);

        $arr_performers['performers_slug'] = $this->TicketEvolutionPublicDisplay()->get_slug_listing_page('performers');
        $arr_performers['venues_slug'] = $this->TicketEvolutionPublicDisplay()->get_slug_listing_page('venues');
        $arr_performers['category_slug'] = get_post(get_option('ticket_evolution_events_option')['page_category_events'])->post_name;
        $arr_performers['get_url_site'] = $arr_performers['get_url_site'];
        $arr_performers['ticket_groups'] = $this->GetApi()->arr_tickets_group_by_id_event($arr_performers['id']);

        $this->TicketEvolutionPublicDisplay()->template('shortcode/events/event_widget', $arr_performers);
    }
    
    public function register_form($param) {
        $params = shortcode_atts(
                [
                    'title' => '',
                ], $param);
        $arr_performers['params'] = $params;
        
        $this->TicketEvolutionPublicDisplay()->template('shortcode/ticketevolution-form/ticketevolution-register-form', $arr_performers);
    }
    
    public function login_form($param) {
        $arr_performers['loginform'] = wp_login_form($this->GetApi()::arg_for_login_form()); //loginform
        
        $this->TicketEvolutionPublicDisplay()->template('shortcode/ticketevolution-form/ticketevolution-login-form', $arr_performers);
    }

}

new TicketEvolution_Shortcode();
