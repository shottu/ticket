
jQuery(window).ready(function () {
////On page listing events
//Filter
    var all_items = [];
    var parameter_filter = [];

    jQuery('.tickevolution.listing .title-event').map(function () {
        all_items.push(jQuery(this));
    });

    updateItemsFilter();

    jQuery('.tickevolution.listing .filter .more').on('click', function () {

        jQuery('.tickevolution.listing .' + this.id + ' .hide').toggle("slow");

        if (jQuery(this).text() == 'More ') {
            jQuery(this).html('Less <i class="fas fa-arrow-up float-right"></i>');
        } else {
            jQuery(this).html('More <i class="fas fa-arrow-down float-right"></i>');
        }
    });

    jQuery('.tickevolution.listing .filter .click').on('click', function () {

        if (jQuery(this).html() == jQuery(this).text() + '<i class="fas fa-check float-right success"></i>') {
            jQuery(this).html(jQuery(this).text());
            parameter_filter.splice(parameter_filter.indexOf(this.id), 1); //delete with array
        } else {
            jQuery(this).html(jQuery(this).text() + ' <i class="fas fa-check float-right success"></i>');
            parameter_filter.push(this.id); //add in array
        }

        filter(parameter_filter, all_items);
    });

    function addFilterItems(name_attr) {
        var arr = [];
        var k = 0;
        var class_hide = 'hide';
        var more = '';

        jQuery('.tickevolution.listing .title-event').map(function () {
            arr.push(jQuery(this).attr('attr-' + name_attr));
        });

        arr = arr.filter(function (elem, index, self) {
            return index === self.indexOf(elem);
        });

        arr.forEach(function (item) {
            k++;
            if (k <= 5) {
                class_hide = '';
                more = '';
            } else {
                class_hide = 'hide';
                more = '<button type="button" id="' + name_attr + '" class="list-group-item list-group-item-action more">More <i class="fas fa-arrow-down float-right"></i></button>';
            }
            jQuery('.tickevolution.listing .filter .' + name_attr).append('<button type="button" id="' + item + '" class="list-group-item list-group-item-action click ' + class_hide + '">' + item + '</button>');

        });
        jQuery('.tickevolution.listing .filter .' + name_attr).append(more);
    }

    function updateItemsFilter() {
        addFilterItems('months');
        addFilterItems('days');
        addFilterItems('vanues');
    }

    function filter(parameter, data) {
        jQuery('.tickevolution.listing .list .items').remove();
        jQuery('.tickevolution.listing .list').append('<div class="items"></div>');
        if (parameter.length > 0) {
            data.forEach(function (item) {
                var arr_attr = [];
                var key = 0;
                arr_attr.push(item.attr('attr-times'), item.attr('attr-days'), item.attr('attr-vanues'), item.attr('attr-months'));

                parameter.forEach(function (item) {
                    arr_attr.forEach(function (item_arr) {
                        if (item == item_arr) {
                            key++;
                        }
                    });
                });

                if (key == parameter.length) {
                    jQuery('.tickevolution.listing .list .items').append(item);
                }
            });
        } else {
            data.forEach(function (item) {
                jQuery('.tickevolution.listing .list .items').append(item);
            });
        }

    }

//end

});


