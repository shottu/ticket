<?php

class TicketEvolutionPageSettingsInvite {

    protected static $_instance = null;

    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct() {
        add_action('admin_menu', array($this, 'add_invite_page'));
        add_action('admin_init', array($this, 'page_init'));
    }

    /**
     * Add options page
     */
    public function add_invite_page() {
        // This page will be under "Settings"
        add_submenu_page(
                'ticket_evolution_setting_admin', 'Generation Invite', 'Invite', 'manage_options', 'ticket_evolution_settings_invite', array($this, 'create_invite_page')
        );
    }

    /**
     * Ensuring there's only one instance
     */
    public static function instance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * Options page callback
     */
    public function create_invite_page() {
        // Set class property
        $this->options = get_option('ticket_evolution_invite_option');
        ?>
        <div class="wrap">
            <h1></h1>
            <form method="post" action="options.php">
                <?php
                // This prints out all hidden setting fields
                settings_fields('ticket_evolution_invite_group');
                do_settings_sections('ticket_evolution_setting_invite');
                submit_button();
                ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init() {
        register_setting(
                'ticket_evolution_invite_group', // Option group
                'ticket_evolution_invite_option', // Option name
                array($this, 'sanitize') // Sanitize
        );

        add_settings_section(
                'ticket_evolution_invite_settings', // ID
                'Ticket Invite Settings', // Title
                array($this, 'print_section_info'), // Callback
                'ticket_evolution_setting_invite' // Page
        );

        add_settings_field(
                'api_version', // ID
                'Api Version', // Title 
                array($this, 'api_version_callback'), // Callback
                'ticket_evolution_setting_invite', // Page
                'ticket_evolution_invite_settings' // Section           
        );

        add_settings_field('api_token', 'Your Api Token', array($this, 'api_token_callback'), 'ticket_evolution_setting_invite', 'ticket_evolution_invite_settings');

        add_settings_field('api_secret', 'Your Api Secret', array($this, 'api_secret_callback'), 'ticket_evolution_setting_invite', 'ticket_evolution_invite_settings');

        add_settings_field('api_environment', 'API Environment', array($this, 'api_environment_callback'), 'ticket_evolution_setting_invite', 'ticket_evolution_invite_settings');
    }

    /**'
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize($input) {
        $new_input = array();
        if (isset($input['api_version']))
            $new_input['api_version'] = sanitize_text_field($input['api_version']);

        if (isset($input['api_token']))
            $new_input['api_token'] = sanitize_text_field($input['api_token']);

        if (isset($input['api_secret']))
            $new_input['api_secret'] = sanitize_text_field($input['api_secret']);

        if (isset($input['api_environment']))
            $new_input['api_environment'] = sanitize_text_field($input['api_environment']);

        return $new_input;
    }

    /**
     * Print the Section text
     */
    public function print_section_info() {
        print 'Enter your settings below:';
    }

    /**
     * Get the settings option array and print one of its values
     */
    public function api_version_callback() {
        printf('<input type="text" id="api_version" name="ticket_evolution_invite_option[api_version]" value="%s" />', isset($this->options['api_version']) ? esc_attr($this->options['api_version']) : 'v9');
    }
    
    public function api_token_callback() {
        printf('<input type="text" id="api_token" name="ticket_evolution_invite_option[api_token]" value="%s" />', isset($this->options['api_token']) ? esc_attr($this->options['api_token']) : '');
    }

    public function api_secret_callback() {
        printf('<input type="text" id="api_secret" name="ticket_evolution_invite_option[api_secret]" value="%s" />', isset($this->options['api_secret']) ? esc_attr($this->options['api_secret']) : '');
    }

    public function api_environment_callback() {

        $items = [
            "SandBox" => "http://api.sandbox.ticketevolution.com",
            "Production" => "http://api.ticketevolution.com",
            "Staging" => "http://api.staging.ticketevolution.com"
        ];

        $api_environment = isset($this->options['api_environment']) ? esc_attr($this->options['api_environment']) : '';

        echo "<select id='api_environment' name='ticket_evolution_invite_option[api_environment]'>";
        foreach ($items as $item => $value) {
            $selected = ($api_environment == $value) ? 'selected="selected"' : '';
            echo "<option value='$value' $selected>$item</option>";
        }
        echo "</select>";
    }
}
