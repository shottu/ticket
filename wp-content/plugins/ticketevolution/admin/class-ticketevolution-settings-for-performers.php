<?php

class TicketEvolutionPageSettingsPerformers {

    protected static $_instance = null;

    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct() {
        add_action('admin_menu', array($this, 'add_performers_page'));
        add_action('admin_init', array($this, 'page_init'));
    }

    /**
     * Add options page
     */
    public function add_performers_page() {
        // This page will be under "Settings"
        add_submenu_page(
                'ticket_evolution_setting_admin', 'Generation Performers', 'Performers', 'manage_options', 'ticket_evolution_settings_performers', array($this, 'create_performers_page')
        );
    }

    /**
     * Ensuring there's only one instance
     */
    public static function instance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * Options page callback
     */
    public function create_performers_page() {
        // Set class property
        $this->options = get_option('ticket_evolution_performers_option');
        ?>
        <div class="wrap">
            <h1></h1>
            <form method="post" action="options.php">
                <?php
                // This prints out all hidden setting fields
                settings_fields('ticket_evolution_performers_group');
                do_settings_sections('ticket_evolution_setting_performers');
                submit_button();
                ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init() {
        register_setting(
                'ticket_evolution_performers_group', // Option group
                'ticket_evolution_performers_option', // Option name
                array($this, 'sanitize') // Sanitize
        );

        add_settings_section(
                'ticket_evolution_performers_settings', // ID
                'Ticket Performers Settings', // Title
                array($this, 'print_section_info'), // Callback
                'ticket_evolution_setting_performers' // Page
        );

        add_settings_field('page_listing_performers', 'Choose Page Listing Performers', array($this, 'page_listing_performers_callback'), 'ticket_evolution_setting_performers', 'ticket_evolution_performers_settings');

        add_settings_field('count_on_listing_performers', 'Number of Performers per page', array($this, 'count_performers_on_listing_page_callback'), 'ticket_evolution_setting_performers', 'ticket_evolution_performers_settings');
    }

    /**'
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize($input) {
        $new_input = array();

        if (isset($input['page_listing_performers']))
            $new_input['page_listing_performers'] = sanitize_text_field($input['page_listing_performers']);

        if (isset($input['count_on_listing_performers']))
            $new_input['count_on_listing_performers'] = sanitize_text_field($input['count_on_listing_performers']);

        return $new_input;
    }

    /**
     * Print the Section text
     */
    public function print_section_info() {
        print 'Enter your settings below:';
    }

    /**
     * Get the settings option array and print one of its values
     */

    public function count_performers_on_listing_page_callback() {
        printf('<input type="number" id="count_on_listing_performers" name="ticket_evolution_performers_option[count_on_listing_performers]" value="%s" />', isset($this->options['count_on_listing_performers']) ? esc_attr($this->options['count_on_listing_performers']) : '30');
    }

    public function page_listing_performers_callback() {

        echo "<select id='page_listing_performers' name='ticket_evolution_performers_option[page_listing_performers]'>";
        echo '<option value="">' . esc_attr(__('Select page')) . '</option>';

        foreach (get_pages() as $page) {

            $selected = (get_option('ticket_evolution_performers_option')['page_listing_performers'] == $page->ID) ? 'selected="selected"' : '';
            echo '<option value="' . $page->ID . '"' . $selected . '>' . esc_html($page->post_title) . '</option>';
        }
        echo "</select>";
    }

}
