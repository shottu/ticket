<?php
$count = $arr_arg['total_entries'] / $arr_arg['pre_page'] + 1;
if ($arr_arg['current_page'] == 1) {
    $disabled = 'disabled';
}
if ($arr_arg['current_page'] == (int) $count) {
    $disabled_next = 'disabled';
}
?>
<nav>
    <ul class="pagination pagination-tickevolution justify-content-center">                            
        <li class="page-item <?= $disabled; ?>">
            <a class="page-link" href="<?= $arr_arg['link_url_site'] . '/' . $arr_arg['slug_listing_page']; ?>/page/<?= $arr_arg['current_page'] - 1; ?>"><i class="fas fa-arrow-left"></i></a>
        </li>        
        <?php if($arr_arg['current_page'] != 1 && $arr_arg['current_page'] >= 3) :?>
            <li class="page-item"><a class="page-link" href="<?= $arr_arg['link_url_site'] . '/' . $arr_arg['slug_listing_page']; ?>/page/1">1</a></li>
            <div class="space-page-link">
                <li class="page-item"><a class="page-link">...</a></li>
            </div>
        <?php endif;?>           
        <?php
        for ($i = 1; $i <= $count; $i++) {
            if ($arr_arg['current_page'] == $i) {
                $active = 'active';
                $current = '<span class="sr-only">(current)</span>';
            } else {
                $active = '';
                $current = '';
            }

            if ($i <= $arr_arg['current_page'] + 1 && $i >= $arr_arg['current_page'] - 1) {
                ?>
                <li class="page-item"><a class="page-link <?= $active; ?>" href="<?= $arr_arg['link_url_site'] . '/' . $arr_arg['slug_listing_page']; ?>/page/<?= $i; ?>"><?= $i; ?><?= $current; ?></a></li>
                <?php
            }
        }
        ?>
        <?php if($arr_arg['current_page'] != (int) $count && $arr_arg['current_page'] <= ((int) $count - 2)) :?>
            <div class="space-page-link">
                <li class="page-item"><a class="page-link">...</a></li>
            </div>
           <li class="page-item"><a class="page-link" href="<?= $arr_arg['link_url_site'] . '/' . $arr_arg['slug_listing_page']; ?>/page/<?= (int) $count?>"><?= (int) $count?></a></li>
        <?php endif;?>            
        <li class="page-item <?= $disabled_next; ?>">
            <a class="page-link" href="<?= $arr_arg['link_url_site'] . '/' . $arr_arg['slug_listing_page']; ?>/page/<?= $arr_arg['current_page'] + 1; ?>"><i class="fas fa-arrow-right"></i>    </a>
        </li>
    </ul>
</nav>

<?php

