<?php

class TicketEvolutionPageSettings {

    protected static $_instance = null;

    /**
     * Holds the values to be used in the fields callbacks
     */
    private $options;

    /**
     * Start up
     */
    public function __construct() {
        add_action('admin_menu', array($this, 'add_plugin_page'));
        add_action('admin_init', array($this, 'page_init'));
    }

    /**
     * Add options page
     */
    public function add_plugin_page() {
        // This page will be under "Settings"
        add_menu_page(
                'Ticket Evolution', 'Ticket Evolution', 'manage_options', 'ticket_evolution_setting_admin', array($this, 'create_admin_page')
        );
    }

    /**
     * Ensuring there's only one instance
     */
    public static function instance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * Options page callback
     */
    public function create_admin_page() {
        // Set class property
        $this->options = get_option('ticket_evolution_option');
        ?>
        <div class="wrap">
            <h1></h1>
            <form method="post" action="options.php">
                <?php
                // This prints out all hidden setting fields
                settings_fields('ticket_evolution_group');
                do_settings_sections('ticket_evolution_setting_admin');
                submit_button();
                ?>
            </form>
        </div>
        <?php
    }

    /**
     * Register and add settings
     */
    public function page_init() {
        register_setting(
                'ticket_evolution_group', // Option group
                'ticket_evolution_option', // Option name
                array($this, 'sanitize') // Sanitize
        );

        add_settings_section(
                'ticket_evolution_settings', // ID
                'Ticket Evolution Settings', // Title
                array($this, 'print_section_info'), // Callback
                'ticket_evolution_setting_admin' // Page
        );

        add_settings_field('page_checkout', 'Choose Page Checkout', array($this, 'page_checkout_callback'), 'ticket_evolution_setting_admin', 'ticket_evolution_settings');
        
         add_settings_field('page_user_profile', 'Choose Page Profile User', array($this, 'page_user_profile_callback'), 'ticket_evolution_setting_admin', 'ticket_evolution_settings');

        add_settings_field('page_login_users', 'Choose Page Login and Registration Users', array($this, 'page_login_users_callback'), 'ticket_evolution_setting_admin', 'ticket_evolution_settings');

        //add_settings_field('sort_events', 'Sort Events', array($this, 'sort_events_callback'), 'ticket_evolution_setting_admin', 'ticket_evolution_settings');
    }

    /**
     * Sanitize each setting field as needed
     *
     * @param array $input Contains all settings fields as array keys
     */
    public function sanitize($input) {
        $new_input = array();

        if (isset($input['page_checkout']))
            $new_input['page_checkout'] = sanitize_text_field($input['page_checkout']);

        if (isset($input['page_login_users']))
            $new_input['page_login_users'] = sanitize_text_field($input['page_login_users']);

        if (isset($input['page_user_profile']))
            $new_input['page_user_profile'] = sanitize_text_field($input['page_user_profile']);

        return $new_input;
    }

    /**
     * Print the Section text
     */
    public function print_section_info() {
        print 'Enter your settings below:';
    }

    /**
     * Get the settings option array and print one of its values
     */

    public function page_checkout_callback() {

        echo "<select id='page_checkout' name='ticket_evolution_option[page_checkout]'>";
        echo '<option value="">' . esc_attr(__('Select page')) . '</option>';

        foreach (get_pages() as $page) {

            $selected = (get_option('ticket_evolution_option')['page_checkout'] == $page->ID) ? 'selected="selected"' : '';
            echo '<option value="' . $page->ID . '"' . $selected . '>' . esc_html($page->post_title) . '</option>';
        }
        echo "</select>";
    }

    public function page_user_profile_callback() {

        echo "<select id='page_user_profile' name='ticket_evolution_option[page_user_profile]'>";
        echo '<option value="">' . esc_attr(__('Select page')) . '</option>';

        foreach (get_pages() as $page) {

            $selected = (get_option('ticket_evolution_option')['page_user_profile'] == $page->ID) ? 'selected="selected"' : '';
            echo '<option value="' . $page->ID . '"' . $selected . '>' . esc_html($page->post_title) . '</option>';
        }
        echo "</select>";
    }

    public function page_login_users_callback() {

        echo "<select id='page_login_users' name='ticket_evolution_option[page_login_users]'>";
        echo '<option value="">' . esc_attr(__('Select page')) . '</option>';

        foreach (get_pages() as $page) {

            $selected = (get_option('ticket_evolution_option')['page_login_users'] == $page->ID) ? 'selected="selected"' : '';
            echo '<option value="' . $page->ID . '"' . $selected . '>' . esc_html($page->post_title) . '</option>';
        }
        echo "</select>";
    }

    /**
     * Get the settings option array
     * @return array
     */
    public function getSettingsPage() {

        $ticket_evolution_option = get_option('ticket_evolution_option');
        
        $page_checkout = isset($ticket_evolution_option['page_checkout']) ? esc_attr($ticket_evolution_option['page_checkout']) : '';
        $page_user_profile = isset($ticket_evolution_option['page_user_profile']) ? esc_attr($ticket_evolution_option['page_user_profile']) : '';

        return $data_setings = [
            'page_checkout' => $page_checkout,
            'page_user_profile' => $page_user_profile,
        ];
    }
    
    public function getSettingsPageApi() {

        $ticket_evolution_option = get_option('ticket_evolution_invite_option');

        $api_secret = isset($ticket_evolution_option['api_secret']) ? esc_attr($ticket_evolution_option['api_secret']) : '';
        $api_token = isset($ticket_evolution_option['api_token']) ? esc_attr($ticket_evolution_option['api_token']) : '';
        $api_version = isset($ticket_evolution_option['api_version']) ? esc_attr($ticket_evolution_option['api_version']) : 'v9';
        $api_environment = isset($ticket_evolution_option['api_environment']) ? esc_attr($ticket_evolution_option['api_environment']) : '';

        return $data_setings = [
            'api_secret' => $api_secret,
            'api_token' => $api_token,
            'api_version' => $api_version,
            'api_environment' => $api_environment,
        ];
    }
}
