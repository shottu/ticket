<div class="<?= $arr_arg['fixed'];?> cart-tickets <?= $arr_arg['show_checkout'];?>" id="shortcode-cart">
    <div class="block-hide-ticket-cart">
        <h4 class="d-flex cours justify-content-between align-items-center mb-3">
            <span class="text-muted">Your cart <i class="fas fa-shopping-cart"></i></span>
            <span class="badge badge-secondary badge-pill count-cart">0</span>
        </h4>
    </div>
<!--    <form method="POST">
        <input type="text" name="promo-code" placeholder="Promo">
        <button name="promo-button">Go</button>
    </form>-->
    <!--    info box for js-->
    <div class="info-box-for-js hide" attr-slug-checkout-page="<?= $arr_arg['link_page_checkout'] ?>"></div>
    <!--end-->
</div>