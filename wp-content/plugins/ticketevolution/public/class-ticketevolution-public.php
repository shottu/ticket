<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    ticketevolution
 * @subpackage ticketevolution/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    ticketevolution
 * @subpackage ticketevolution/public
 * @author     Your Name <email@example.com>
 */
class TicketEvolution_Public {

    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $ticketevolution    The ID of this plugin.
     */
    private $ticketevolution;

    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;

    /**
     * Initialize the class and set its properties.
     *
     * @since    1.0.0
     * @param      string    $ticketevolution       The name of the plugin.
     * @param      string    $version    The version of this plugin.
     */
    public function __construct($ticketevolution, $version) {

        $this->ticketevolution = $ticketevolution;
        $this->version = $version;
    }

    /**
     * Register the stylesheets for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_styles() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in ticketevolution_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The ticketevolution_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_enqueue_style($this->ticketevolution, plugin_dir_url(__FILE__) . 'css/ticketevolution-public.css', array(), $this->version, 'all');
        wp_enqueue_style('bootstrap-grid', plugin_dir_url(__FILE__) . 'css/bootstrap-grid.min.css');
        wp_enqueue_style('bootstrap', plugin_dir_url(__FILE__) . 'css/bootstrap.min.css');
        wp_enqueue_style('fontawsome', plugin_dir_url(__FILE__) . 'css/all.min.css');
        wp_enqueue_style('awesome', 'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css');
        wp_enqueue_style('toast', plugin_dir_url(__FILE__) . 'css/jquery.toast.css');
        wp_enqueue_style('ticketevolution-custom', plugin_dir_url(__FILE__) . 'css/ticketevolution.css');
        wp_enqueue_style('ticketevolution-public-responsive', plugin_dir_url(__FILE__) . 'css/ticketevolution-public-responsive.css');
    }

    /**
     * Register the JavaScript for the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts() {

        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in ticketevolution_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The ticketevolution_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_enqueue_script($this->ticketevolution, plugin_dir_url(__FILE__) . 'js/ticketevolution-public.js', array('jquery'), $this->version, false);
        wp_enqueue_script('filter', plugin_dir_url(__FILE__) . 'js/filter.js');
        wp_enqueue_script('tevomaps', plugin_dir_url(__FILE__) . 'js/tevomaps.js', array('jquery'), 1, false);
        wp_enqueue_script('bootstrap', plugin_dir_url(__FILE__) . 'js/bootstrap.min.js', array('jquery'), 4);
        wp_enqueue_script('bundle', plugin_dir_url(__FILE__) . 'js/bootstrap.bundle.min.js');
        wp_enqueue_script('popper', 'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js');
        wp_enqueue_script('jquery_cookie', plugin_dir_url(__FILE__) . 'js/jquery_cookie.js');
        wp_enqueue_script('toast', plugin_dir_url(__FILE__) . 'js/jquery.toast.js');
    }

    function settings_css() {

        echo '<style type="text/css">';

        echo '.tickevolution .btn {';
        echo 'background-color: ' . get_option('ticket_evolution_style_option')['color_btn'] . ';';
        echo 'border-color:' . get_option('ticket_evolution_style_option')['color_btn'] . ';';
        echo '}';

        echo '.tickevolution .list-group-item.active {';
        echo 'background-color: ' . get_option('ticket_evolution_style_option')['color_primery'] . ';';
        echo 'border-color:' . get_option('ticket_evolution_style_option')['color_primery'] . ';';
        echo '}';
        echo 'html .pagination.pagination-tickevolution .active {';
        echo 'background: ' . get_option('ticket_evolution_style_option')['color_primery'] . ';';
        echo '}';
        echo 'html .pagination.pagination-tickevolution .page-link {';
        echo 'color: ' . get_option('ticket_evolution_style_option')['color_primery'] . ' !important;';
        echo '}';
        echo 'html .pagination.pagination-tickevolution .page-link.active {';
        echo 'color: #fff !important;';
        echo '}';
        echo '.tickevolution .badge {';
        echo 'background-color: ' . get_option('ticket_evolution_style_option')['color_primery'] . ';';
        echo '}';


        echo '.tickevolution .alert {';
        echo 'background-color: ' . get_option('ticket_evolution_style_option')['color_background'] . ';';
        echo 'border-color:' . get_option('ticket_evolution_style_option')['color_background'] . ';';
        echo 'color:' . get_option('ticket_evolution_style_option')['color_text'] . ';';
        echo '}';
        echo '.tickevolution .list-group-item-action {';
        echo 'color:' . get_option('ticket_evolution_style_option')['color_text'] . ';';
        echo '}';

        echo 'html .tickevolution .gray {';
        echo 'color:' . get_option('ticket_evolution_style_option')['secondary_color_text'] . ' !important;';
        echo '}';

        echo '</style>';
    }

    function clear_cookie() {
        if (!empty($_COOKIE['tickets_user'])) {
            setcookie('tickets_user', '', 1);
        }
    }

}
