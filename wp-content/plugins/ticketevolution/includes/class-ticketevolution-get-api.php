<?php

/**
 * Get all info with TicketEvolution API
 *
 *
 * @package    ticketevolution
 * @subpackage ticketevolution/includes
 * 
 */
class GetApi {

    public function GetApi() {

        return new TicketEvolutionConnectApi();
    }

    public function TicketevolutionPublicDisplay() {

        return new TicketevolutionPublicDisplay();
    }

    /**
     * This function get events with API
     * 
     * @link https://ticketevolution.atlassian.net/wiki/spaces/API/pages/9470092/Events+Index description
     * 
     * @param type $page
     * @param type $per_page
     * @param type $order_by        Pass in the parameter that you want to order by and the sorting order (Ex. :order_by => "events.popularity_score DESC")
     * @param type $occurs_at_gte   Refine dates at which the event occurs using an ISO 8601 formatted datetime.
     * @para type $occurs_at_lt     You can use occurs_at twice when using conditionals to retrieve events within a date range. For example, to retrieve only events within April 2018 use occurs_at.gte=2018-04-01&occurs_at.lt=2018-05-01
     * @return type
     */
    public function get_show_list_events($page = 1, $per_page = 100, $order_by = 'events.occurs_at ASC', $occurs_at_gte = '', $occurs_at_lt = '2099-01-18T11:45:00+01:00') {

        if ($occurs_at_gte == '') {
            $occurs_at_gte = $this->TicketevolutionPublicDisplay()::current_date_iso_8601();
        }

        $arr = [
            'page' => $page,
            'per_page' => $per_page,
            'order_by' => $order_by,
            'occurs_at.gte' => $occurs_at_gte,
            'occurs_at.lt' => $occurs_at_lt
        ];
        try {
//            var_dump(http_response_code(504));
            return $this->GetApi()->connectedApiTicket()->listEvents($arr);
        } catch (Exception $e) {
            if(preg_match("/504 Gateway Time-out/i", $e->getMessage())) {
                echo 'Sorry, API Ticketevolution is not working at the moment. Server response "504 Gateway Time-out"';
            }
        }

//        return $this->GetApi()->connectedApiTicket()->listEvents($arr);
    }

    public function get_search_list_events($page = 1, $per_page = 100, $order_by = 'events.occurs_at ASC', $performer_id = 1) {

        return $this->GetApi()->connectedApiTicket()->searchEvents([
                    'page' => $page,
                    'per_page' => $per_page,
                    'order_by' => $order_by,
                    'performer_id' => $performer_id,
        ]);
    }

    public function get_search_list_events_by_venue_id($page = 1, $per_page = 100, $order_by = 'events.occurs_at ASC', $venue_id) {

        return $this->GetApi()->connectedApiTicket()->searchEvents([
                    'page' => $page,
                    'per_page' => $per_page,
                    'order_by' => $order_by,
                    'venue_id' => $venue_id,
        ]);
    }

    public function get_search_by_id($page, $id) {

        if ($page == "event") {
            return $this->GetApi()->connectedApiTicket()->showEvent([
                        'event_id' => (int) $id,
            ]);
        }

        if ($page == "performer") {
            return $this->GetApi()->connectedApiTicket()->showPerformer([
                        'performer_id' => (int) $id,
            ]);
        }

        if ($page == "venue") {
            return $this->GetApi()->connectedApiTicket()->showVenue([
                        'venue_id' => (int) $id,
            ]);
        }
    }

    public function get_search_event_by_category_id($page = 1, $per_page = 100, $category_id) {

        return $this->GetApi()->connectedApiTicket()->listEvents([
                    'page' => $page,
                    'per_page' => $per_page,
                    'categoru' => $category_id
        ]);
    }

    public function get_show_list_performers($page = 1, $per_page = 100, $order_by = 'performers.occurs_at ASC') {

        return $this->GetApi()->connectedApiTicket()->listPerformers([
                    'page' => (int) $page,
                    'per_page' => $per_page,
                    'order_by' => $order_by,
        ]);
    }

    public function get_show_list_venues($page = 1, $per_page = 100, $order_by = 'venues.occurs_at ASC') {

        return $this->GetApi()->connectedApiTicket()->listVenues([
                    'page' => (int) $page,
                    'per_page' => $per_page,
                    'order_by' => $order_by,
        ]);
    }

    public function arr_arg_for_events_listing($page = 1, $per_page = 100, $order_by = 'events.occurs_at ASC') {

        return $arr_arg_for_events_listing = [
            'arr_events' => $this->get_show_list_events($page, $per_page, $order_by)['events'],
            'get_url_site' => $this->TicketevolutionPublicDisplay()->get_url_site(),
            'get_slug_listing_event_page' => $this->TicketevolutionPublicDisplay()->get_slug_listing_page('events'),
            'get_slug_listing_venues_page' => $this->TicketevolutionPublicDisplay()->get_slug_listing_page('venues'),
            'get_slug_listing_performers_page' => $this->TicketevolutionPublicDisplay()->get_slug_listing_page('performers'),
            'total_entries' => $this->get_show_list_events($page, $per_page, $order_by)['total_entries'],
            'pre_page' => $per_page,
            'arr_for_pagination' => [
                'total_entries' => $this->get_show_list_events($page, $per_page, $order_by)['total_entries'],
                'pre_page' => $per_page,
                'slug_listing_page' => $this->TicketevolutionPublicDisplay()->get_slug_listing_page('events'),
                'link_url_site' => $this->TicketevolutionPublicDisplay()->get_url_site(),
            ]
        ];
    }

    public function arr_arg_for_performers_listing($page = 1, $per_page = 100, $order_by = 'performers.popularity_score DESC') {

        return $arr_arg_for_performers_listing = [
            'arr_performers' => $this->get_show_list_performers($page, $per_page, $order_by)['performers'],
            'get_url_site' => $this->TicketevolutionPublicDisplay()->get_url_site(),
            'get_slug_listing_event_page' => $this->TicketevolutionPublicDisplay()->get_slug_listing_page('events'),
            'get_slug_listing_performers_page' => $this->TicketevolutionPublicDisplay()->get_slug_listing_page('performers'),
            'get_slug_listing_venues_page' => $this->TicketevolutionPublicDisplay()->get_slug_listing_page('venues'),
            'total_entries' => $this->get_show_list_performers($page, $per_page, $order_by)['total_entries'],
            'arr_for_pagination' => [
                'total_entries' => $this->get_show_list_performers($page, $per_page, $order_by)['total_entries'],
                'pre_page' => $per_page,
                'slug_listing_page' => $this->TicketevolutionPublicDisplay()->get_slug_listing_page('performers'),
                'link_url_site' => $this->TicketevolutionPublicDisplay()->get_url_site(),
            ]
        ];
    }

    public function arr_arg_for_venues_listing($page = 1, $per_page = 100, $order_by = 'venues.popularity_score DESC') {

        return $arr_arg_for_venues_listing = [
            'arr_venues' => $this->get_show_list_venues($page, $per_page, $order_by)['venues'],
            'get_url_site' => $this->TicketevolutionPublicDisplay()->get_url_site(),
            'get_slug_listing_event_page' => $this->TicketevolutionPublicDisplay()->get_slug_listing_page('events'),
            'get_slug_listing_performers_page' => $this->TicketevolutionPublicDisplay()->get_slug_listing_page('venues'),
            'get_slug_listing_venues_page' => $this->TicketevolutionPublicDisplay()->get_slug_listing_page('venues'),
            'total_entries' => $this->get_show_list_venues($page, $per_page, $order_by)['total_entries'],
            'arr_for_pagination' => [
                'total_entries' => $this->get_show_list_venues($page, $per_page, $order_by)['total_entries'],
                'pre_page' => $per_page,
                'slug_listing_page' => $this->TicketevolutionPublicDisplay()->get_slug_listing_page('venues'),
                'link_url_site' => $this->TicketevolutionPublicDisplay()->get_url_site(),
            ]
        ];
    }

    public function arr_tickets_group_by_id_event($event_id) {

        $arr = $this->GetApi()->connectedApiTicket()->listTicketGroups([
            'event_id' => (int) $event_id,
            'lightweight' => true,
            'include_tevo_section_mappings' => true,
            'ticket_list' => true
        ]);
        foreach ($arr['ticket_groups'] as $key) {
            $obj[] = [
                'tevo_section_name' => $key['tevo_section_name'],
                'retail_price' => $key['retail_price'],
                'id' => $key['id'],
                'format' => $key['format'],
                'section' => $key['section'],
                'signature' => $key['signature'],
                'type' => $key['type'],
                'office_name' => $key['office']['name'],
                'office_id' => $key['office']['id'],
                'public_notes' => $key['public_notes'],
                'row' => $key['row'],
                'available_quantity' => $key['available_quantity']
            ];
        }

        if(!empty($obj)) {
            return $obj;
        }
    }
    
    public function error($param) {
        $error = '<div class="alert alert-danger" role="alert">'.$param.'</div>';
        return $error;
    }

    public function get_ticket_groups($id_ticket_group) {
        $ticket_group = $this->GetApi()->connectedApiTicket()->showTicketGroup([
            'ticket_group_id' => (int) $id_ticket_group,
            'ticket_list' => true
        ]);

        return $ticket_group;
    }

    public function get_list_service_fee() {
        return $this->GetApi()->connectedApiTicket()->listServiceFeesSettings();
    }

    public function get_delivery_option() {
        return $this->GetApi()->connectedApiTicket()->listShippingSettings();
    }

    public function get_promotion_code() {
        return $this->GetApi()->connectedApiTicket()->listPromotionCodes();
    }

    public function get_client_info($client_id) {
        try {
            return $this->GetApi()->connectedApiTicket()->showClient([
                    'client_id' => $client_id
            ]);
        } catch (Exception $e) {
            if(preg_match('/400 Bad Request/i', $e->getMessage())) {
            echo $this->error('400 Bad Request');
            }
        }
    }

    public function get_client_credite_cards($client_id) {
        return $this->GetApi()->connectedApiTicket()->listClientCreditCards([
                    'client_id' => $client_id
        ]);
    }

    public function get_show_tickets($ticket_id) {
        return $this->GetApi()->connectedApiTicket()->showTicket([
                    'ticket_id' => (int) $ticket_id
        ]);
    }

    public function get_list_orders($page = 1, $per_page = 100) {
        return $this->GetApi()->connectedApiTicket()->listOrders([
                    'page' => $page,
                    'per_page' => $per_page,
        ]);
    }

    public function arr_configuration_by_id_venue($venue_id, $page = 1, $per_page = 100) {

        return $this->GetApi()->connectedApiTicket()->listConfigurations([
                    'venue_id' => (int) $venue_id,
                    'page' => $page,
                    'per_page' => $per_page,
        ]);
    }

    public static function get_cookies() {

        $vowels = array(' h', '\"', "]", "[");
        if (!empty($_COOKIE['tickets_user'])) {
            $cookies = explode("},", str_replace($vowels, "", $_COOKIE['tickets_user']));
            
            foreach ($cookies as $value) {
                $value_arr = explode(",", $value);
                foreach ($value_arr as $el) {
                    $el_arr = explode(":", $el);
                    if ($el_arr[0] == '[{id' || $el_arr[0] == '{id') {
                        $el_arr[0] = 'id';
                    }
                    $arr[$el_arr[0]] = $el_arr[1];
                }
                $cookies_arr[] = $arr;
            }
            return $cookies_arr;
        }
    }

    public static function arg_for_login_form() {
        return $arg = [
            'echo' => false,
            'redirect' => site_url($_SERVER['REQUEST_URI']),
            'form_id' => 'loginform',
            'label_username' => __('Username'),
            'label_password' => __('Password'),
            'label_remember' => __('Remember Me'),
            'label_log_in' => __('Log In'),
            'id_username' => 'user_login',
            'id_password' => 'user_pass',
            'id_remember' => 'rememberme',
            'id_submit' => 'wp-submit',
            'remember' => true,
            'value_username' => NULL,
            'value_remember' => true
        ];
    }

}
