<div class="col-md-6">
    <h4><?= $arr_arg['params']['title']; ?></h4>
    <form id="registerform" action="<?php echo site_url('wp-login.php?action=register'); ?>" method="post">
        <p>
            <label for="user_login">
                Name<br>
                <input type="text" name="user_login" id="user_login" class="input" value="" size="25" style="">
            </label>
        </p>
        <p>
            <label for="user_email">
                E-mail<br>
                <input type="email" name="user_email" id="user_email" class="input" value="" size="25">
            </label>
        </p>
        <p id="reg_passmail">Registration confirmation will be sent to your e-mail.</p>
        <input type="hidden" name="redirect_to" value="">
        <p class="submit"><input type="submit" name="wp-submit" id="wp-submit" class="button button-primary button-large" value="Registration"></p>
    </form>
</div>
