<?php
/*
	One wp-config for local development to staging to production.
*/
// Define Environments
$environments = array(
    'development' => 'ticketevolution.local',
    'staging' => 'ticketevolution.devos-software.com',
    'production' => 'ticketevolution.com',
);
// Get Server name
$server_name = $_SERVER['SERVER_NAME'];
 
foreach($environments AS $key => $env){
    if(strstr($server_name, $env)){
        define('ENVIRONMENT', $key);
        break;
    }
}
 
// If no environment is set default to production
if(!defined('ENVIRONMENT')) define('ENVIRONMENT', 'production');
// Define different DB connection details depending on environment
switch(ENVIRONMENT){
 
    case 'development':
    	define('DB_NAME', 'ticketevolution');
        define('DB_USER', 'root');
        define('DB_PASSWORD', 'root');
        define('DB_HOST', 'localhost');
        define('WP_SITEURL', 'https://ticketevolution.local');
        define('WP_HOME', 'https://ticketevolution.local');
        define('WP_DEBUG', false);
        define('WP_CACHE', false );
        break;
    case 'staging':
        define('DB_NAME', 'devossof_ticketevolution');
        define('DB_USER', 'devossof_ticketevolution');
        define('DB_PASSWORD', '*.0ax!W?x(9P');
        define('DB_HOST', 'localhost');
        define('DB_HOST_SLAVE', 'localhost' );
        define('DB_CHARSET', 'utf8');
	define('DB_COLLATE', 'utf8_unicode_ci');
	define('WP_DEBUG', false );
	define( 'WP_CACHE', false );
        break;
 
    case 'production':
 
        define('DB_NAME', 'DBNAME');
        define('DB_USER', 'DBUSER');
        define('DB_PASSWORD', 'PASSWORD');
        define('DB_HOST', '127.0.0.1');
        define('DB_HOST_SLAVE', '127.0.0.1' );
        define('DB_CHARSET', 'utf8');
	define('DB_COLLATE', 'utf8_unicode_ci');
        define('WP_SITEURL', 'http://mysite.com/');
        define('WP_HOME', 'http://mysite.com/');
        define('WP_DEBUG', false);
        define('WP_CACHE', TRUE );
        break;
}
 
$table_prefix = 'devoswp_';
// Do you speak my language? e.g: 'en_GB';
define('WPLANG', 'en_GB');
// Authentication Unique Keys and Salts. 
// {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
define('AUTH_KEY',         '{2D-o5P8I%%w@i8X50z6W,*aW}f]lZUJU6D)bq-ExD>H-V8Oa&!c!]|xm/|?_5Kp');
define('SECURE_AUTH_KEY',  '+DY>XY<sJcanhtH;CJZN-Q8>7l1=Kcs3*qvBF~8JZ=$TI4!wi`d+nD5rgivM$Z0Z');
define('LOGGED_IN_KEY',    ' ;ao3|yAAEE&;^<h+Zg@5O-5{xzn_0lfsI 0=YEA+hItSH!mWHB0C*ai~aiMw;(p');
define('NONCE_KEY',        'gb!iu-,dFi{a+f[ZgyNJ@8Dk2sH44z+oa+gTR(vh-O`)Y=R-;|6;vupn#_}TA|9W');
define('AUTH_SALT',        'IW;ppF1G<zl %>]s|CoPn`+}A~~j|Jtn,Z(O+-$6 632<b3A=t8S:EU-mp3I`uKJ');
define('SECURE_AUTH_SALT', '9H0hb$w|Xzzd|#x,|wSi;o0S,<+B^fs[xY6Prkfy1Fhv:T/3+Zt>JAm4M,)G4c-^');
define('LOGGED_IN_SALT',   '3+n- v7a&4X)gkY{Oh6]gEPa~3=7HapA;<mN-`L%4q/(l,OZ]w>Y%RAF5.?X_r{?');
define('NONCE_SALT',       'JX)i-<<o|azJVK|pu-t/#[8mA5**3c?coW8+SbzAh1--]]6Sw3(|)+u90;!(,mTL');
# That's It. Pencils down
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');
require_once(ABSPATH . 'wp-settings.php');