<?php if ($_COOKIE['tickets_user']) : ?>
    <?php if (!is_user_logged_in()) : ?>
        <div class="alert alert-danger" role="alert">
            Sorry, you guest:
            <a href="<?= get_post(get_option('ticket_evolution_option')['page_login_users'])->post_name; ?>" class="alert-link">
                Login
            </a>
        </div>
    <?php else : ?>
        <div class="tickevolution ticketevolution-checkout">
            <div class="row">
                <div class="col-md-8">
                    <h4 class="mb-3"><i class="fas fa-map"></i> Billing address</h4>
                    <form method="POST" class="needs-validation" novalidate>
                        <div class="row">
                            <div class="col-md-4 mb-3">
                                <label for="validationCustom01">First name</label>
                                <input type="text" name="checkout-form[first_name]" class="form-control" id="validationCustom01" placeholder="" value="" required style="background-image: url(&quot;data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAABHklEQVQ4EaVTO26DQBD1ohQWaS2lg9JybZ+AK7hNwx2oIoVf4UPQ0Lj1FdKktevIpel8AKNUkDcWMxpgSaIEaTVv3sx7uztiTdu2s/98DywOw3Dued4Who/M2aIx5lZV1aEsy0+qiwHELyi+Ytl0PQ69SxAxkWIA4RMRTdNsKE59juMcuZd6xIAFeZ6fGCdJ8kY4y7KAuTRNGd7jyEBXsdOPE3a0QGPsniOnnYMO67LgSQN9T41F2QGrQRRFCwyzoIF2qyBuKKbcOgPXdVeY9rMWgNsjf9ccYesJhk3f5dYT1HX9gR0LLQR30TnjkUEcx2uIuS4RnI+aj6sJR0AM8AaumPaM/rRehyWhXqbFAA9kh3/8/NvHxAYGAsZ/il8IalkCLBfNVAAAAABJRU5ErkJggg==&quot;); background-repeat: no-repeat; background-attachment: scroll; background-size: 16px 18px; background-position: 98% 50%; cursor: pointer;">
                                <div class="invalid-feedback">
                                    Valid first name is required.
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="validationCustom02">Last name</label>
                                <input type="text"  name="checkout-form[last_name]" class="form-control" id="validationCustom02" placeholder="" value="" required="">
                                <div class="invalid-feedback">
                                    Valid last name is required.
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="validationCustom03">Company (option)</label>
                                <input type="text"  name="checkout-form[company]" class="form-control" id="validationCustom03" placeholder="" value="" required="">
                                <div class="invalid-feedback">
                                    Valid company name is required.
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-7 mb-3">
                                <label for="validationCustom04">Street Address</label>
                                <input type="text"  name="checkout-form[street_address]" class="form-control" id="validationCustom04" placeholder="1234 Main St" required="">
                                <div class="invalid-feedback">
                                    Please enter your shipping address.
                                </div>
                            </div>
                            <div class="col-md-5 mb-3">
                                <label for="validationCustom05">Apt, suite,etc (option)</label>
                                <input type="text"  name="checkout-form[apt]" class="form-control" id="validationCustom06" placeholder="" value="" required="">
                                <div class="invalid-feedback">
                                    Valid last name is required.
                                </div>
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="validationCustom07">City</label>
                            <input type="text"  name="checkout-form[city]" class="form-control" id="validationCustom07" placeholder="" value="" required="">
                            <div class="invalid-feedback">
                                Valid last name is required.
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="validationCustom08">Email <span class="text-muted">(Optional)</span></label>
                            <input type="email"  name="checkout-form[email]" class="form-control" id="validationCustom08" placeholder="you@example.com" required="">
                            <div class="invalid-feedback">
                                Please enter a valid email address for shipping updates.
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 mb-3">
                                <label for="validationCustom09">Country</label>
                                <input type="text"  name="checkout-form[country]" class="form-control" id="validationCustom09" placeholder="United States" value="" required="">
                                <div class="invalid-feedback">
                                    Please enter your a valid country.
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="validationCustom010">State</label>
                                 <input type="text"  name="checkout-form[region]" class="form-control" id="validationCustom010" placeholder="California" value="" required="">
                                <div class="invalid-feedback">
                                    Please enter your a valid state.
                                </div>
                            </div>
                            <div class="col-md-4 mb-3">
                                <label for="validationCustom011">Zip</label>
                                <input  name="checkout-form[zip]" type="text" class="form-control" id="validationCustom011" placeholder="" required="">
                                <div class="invalid-feedback">
                                    Zip code required.
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6 mb-3">
                                <label for="validationCustom012">Phone Number</label>
                                <input type="text"  name="checkout-form[phone_number]" class="form-control" id="validationCustom012" placeholder="" value="" required="">
                                <div class="invalid-feedback">
                                    Valid last name is required.
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="validationCustom013">PO Number (option)</label>
                                <input type="text"  name="checkout-form[po_number]" class="form-control" id="validationCustom013" placeholder="" value="" required="">
                                <div class="invalid-feedback">
                                    Valid last name is required.
                                </div>
                            </div>
                        </div>
                        <div class="custom-control custom-checkbox">
                            <input type="checkout-form[checkbox]" class="custom-control-input" id="save-info">
                            <label class="custom-control-label" for="save-info">Upgrade if requested seats are not available </label>
                        </div>
                        <h4 class="mb-3"><i class="far fa-credit-card"></i> Pay with card</h4>
                        <div class="row">
                            <!--                    form stripe-->
                            <div id="stripeContainer"></div>
                            <!--                    end-->
                        </div>
                        <p>
                            <button class="btn btn-primary btn-lg btn-block">Continue to checkout</button>
                        </p>
                    </form>
                </div>
                <div class="col-md-4">                                   
                    <?= do_shortcode('[cart fixed="true" show_btn_redirect_checkout="true"]') ?>
                </div>
            </div>
        </div>
    <?php endif; ?>

<?php else : ?>
    <div class="alert alert-danger" role="alert">
        Sorry, cart is empty:
        <a href="/<?= get_option('ticket_evolution_option')["page_listing_events"]->post_name ?>" class="alert-link">Events</a>
    </div>
<?php endif; ?>

<script src="https://api.ticketevolution.com/tevo_stripe.js"></script>

<script>
    cardFormCallback = function (result) {
        console.log(result);
    };

    tevoStripe = TevoStripe('stripeContainer', cardFormCallback, {});

    document.addEventListener("DOMContentLoaded", function (event) {
        console.log(tevoStripe.mount());
    });

    jQuery(window).ready(function () {
        jQuery('.tickevolution-event ul.list-tikets').show();
    });

</script>

<!--Riskified-->
<script type="text/javascript">
    //<![CDATA[
    (function () {
        function riskifiedBeaconLoad() {
            var store_domain = 'ticketevolution.com';
            var session_id = 'SESSION ID GOES HERE - as passed to Order->cart_token';
            var url = ('https:' == document.location.protocol ? 'https://' : 'http://')
                    + "beacon.riskified.com?shop=" + store_domain + "&sid=" + session_id;
            var s = document.createElement('script');
            s.type = 'text/javascript';
            s.async = true;
            s.src = url;
            var x = document.getElementsByTagName('script')[0];
            x.parentNode.insertBefore(s, x);
        }
        if (window.attachEvent)
            window.attachEvent('onload', riskifiedBeaconLoad)
        else
            window.addEventListener('load', riskifiedBeaconLoad, false);
    })();
    //]]>
</script>
<!--end-->